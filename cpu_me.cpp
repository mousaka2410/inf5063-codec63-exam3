#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#include <mmintrin.h> //MMX
#include <xmmintrin.h> //sse
#include <emmintrin.h> //sse2
#include <tmmintrin.h> //ssse3
#include <immintrin.h> //avx/avx2

#include <omp.h> //openMP

#include "c63.h"

/* Motion estimation for 8x8 block */
void cpu_me_block_8x8(int w, int h, int range, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, struct macroblock *mb)
{
  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  //start sse
   /*
    antar at left - right kan deles på 8
    hvis ikke må resterende funksjoner kall gjøres under x loop

  */
  uint8_t *block1, *block2;
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (32))), dirty_hack2[8]  __attribute__ ((aligned (32)));
  
  
  __m128i sum;
  //__m128i load1,load2,load3,load4,load5,load6,load7,load8;
  
  uint16_t get_res[16]  __attribute__ ((aligned (32)));//holds the sum after the store operation
  
  
  int stride_r = w;
  //register -5ms
  register int res =0;
  
  /*
     #######Write to stack memory######
     Have little effect on totel time
  */
  
  block1 = orig + my*w+mx;
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  
  for (y = top; y < bottom; ++y)
    {
      //_mm_prefetch(ref + y*w,1); //no effect
      //loop unrolling med 4: 68-69ms med 8: 69ms 4 bedre.. Men sparer bare 1 ms på dette...
      for (x = left; x < right; x++)
	{
	  
	  
      //Start BLOKK1 if we do loop unrolling, but it hase very little payoff
	  res =0;
	  /*
	     #######Write to stack memory######
	     totel time: 21ms
	  */
	  
	  block2 = ref + y*w + x; 
	  //__builtin_prefetch(ref + (y)*w+x,2);
	  dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
	  dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
	  dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
	  dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
	  
	 
	   
	  /*
	         #######Calculations#######
		 total time: 28ms
	  */
	  
	  sum = _mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[0]) , _mm_loadu_si128((__m128i*)&dirty_hack2[0]));
	  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[2]),_mm_loadu_si128((__m128i*)&dirty_hack2[2]))); 
	  sum = _mm_adds_epu16(sum, _mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[4]), _mm_loadu_si128((__m128i*)&dirty_hack2[4])));
	  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[6]) ,_mm_loadu_si128((__m128i*)&dirty_hack2[6])));
	  
	
	   
	  /*
	    writeout
	    total time 8ms
	  */
	  
	  //+8ms
	  _mm_store_si128((__m128i*)&get_res[0],sum);
	  
	  //+2ms
	  res += get_res[0]; 
	  res += get_res[4]; 
	  
	  if (res < best_sad)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad = res;
	    }
	  
	  //END BLOKK1
	  
	  
	  
	  /*
	  //TEST
	  int sad;
	  
	  sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
	  if(sad != res) {
	  printf("me_block: %d  %d\n",sad,res);
	  
	  }
	  */
	  
	  /* printf("(%4d,%4d) - %d\n", x, y, sad); */
	  
	}
    }
  


  //end sse

#define PRINT_SAD 0
#if PRINT_SAD
      printf("sad: (%d,%d) %d\n", mb_x, mb_y, best_sad);
#endif
#undef PRINT_SAD


  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}

