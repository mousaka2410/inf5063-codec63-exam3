//tar bare med alle fordi jeg er lat
#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cuda.h>

#include "c63.h" //for yuv_t
#include "common.h"
#include "siscilib.h"
#include "stdcuda.h"
#include "read_yuv.h"

#define RYDEBUG 1

static uint32_t width;
static uint32_t height;

typedef struct transferTest transferTest;
struct transferTest {
  int test[5];
};

 

void init_read_yuv(uint32_t w, uint32_t h) {
  width = w; height = h;
}

void send_yuv(c63_common *cm, yuv_t* image, int frameNo, clientParams *masterCli, int topRow, int nRows){
  if (!image) { return; }
  size_t Y_size,U_size,V_size,total_size,td_size,offset;
  uint8_t * memory; //continues memory for siscilib
  transferData *td;

  td_size = sizeof(transferData);
  Y_size = cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT];
  U_size = cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT];
  V_size = cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT];
  total_size = Y_size + U_size + V_size + td_size;

  size_t reconsY_size = 0;
  size_t reconsU_size = 0;
  size_t reconsV_size = 0;
  
  if (cm->frames_since_keyframe != cm->keyframe_interval && cm->framenum != 0){
    // must add the reconstructed image, since important parts of it is calculated on only this side
    reconsY_size = getBufSize(cm, recons, Y_COMPONENT);
    reconsU_size = getBufSize(cm, recons, U_COMPONENT);
    reconsV_size = getBufSize(cm, recons, V_COMPONENT);
    total_size += reconsY_size + reconsU_size + reconsV_size;
  }

#if RYDEBUG
  total_size +=sizeof(transferTest);
#endif
  
  CUDA_ERROR( cudaMallocHost(&memory,total_size));
  td = (transferData *)memory;
  td->size_Y = Y_size; td->size_U=U_size; td->size_V=V_size; td->frameNo = frameNo;
  td->r_size_Y = reconsY_size; td->r_size_U = reconsU_size; td->r_size_V = reconsV_size;
  td->width = width; td->height = height; td->totalSize = total_size;
  td->type = DATA;

  // DONE: added td->topRow and td->nRows.
  td->topRow = topRow;
  td->nRows  = nRows;

#if RYDEBUG
  fprintf(stderr,"read_yuv_master: transferData\n");
  fprintf(stderr,"totalSize: %d\n",td->totalSize);
  fprintf(stderr,"size_Y:    %d\n",td->size_Y);
  fprintf(stderr,"size_U:    %d\n",td->size_U);
  fprintf(stderr,"size_V:    %d\n",td->size_V);
  fprintf(stderr,"width:     %d\n",td->width);
  fprintf(stderr,"height:    %d\n",td->height);
  fprintf(stderr,"frameNo:   %d\n",td->frameNo);
#endif
  
  //suboptimal  bruk av minne, men det isolerer com delen av koden fra resten av systemet
  offset = td_size;
  memcpy(memory+offset,image->Y,Y_size); offset+=Y_size;
  memcpy(memory+offset,image->U,U_size); offset+=U_size;
  memcpy(memory+offset,image->V,V_size); offset+=V_size;

  if (cm->frames_since_keyframe != cm->keyframe_interval && cm->framenum != 0){
    // copies the reconstructed image
    memcpy(memory+offset, cm->curframe->reconsMerge->Y, reconsY_size); offset += reconsY_size;
    memcpy(memory+offset, cm->curframe->reconsMerge->U, reconsU_size); offset += reconsU_size;
    memcpy(memory+offset, cm->curframe->reconsMerge->V, reconsV_size); offset += reconsV_size;
  }

#if RYDEBUG
  transferTest tT;
  tT.test[0] =1; tT.test[1] =2; tT.test[2] =3;
  memcpy(memory+offset,&tT,sizeof(transferTest));
#endif

  SISCI_ERROR(sendData((uint8_t*)memory,total_size,masterCli));
  CUDA_ERROR(cudaFreeHost(memory));
}


/*
Read planar YUV frames with 4:2:0 chroma sub-sampling, send frames to read_yuv_slave
Allocates contiunes memory, must be dealloced by one cudaFree.
Tested: NO
Bugs:
Comments:
*/
bool read_yuv_master(FILE *file,struct c63_common *cm,int frameNo,clientParams *masterCli)
{
  size_t len = 0;
  //yuv_t *image =(yuv_t *)malloc(sizeof(*image));
  yuv_t *image = cm->curframe->orig;

  size_t Y_size,U_size,V_size;

  Y_size = cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT];
  U_size = cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT];
  V_size = cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT];

  //image->Y = (uint8_t*)calloc(1, cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT]);
/*
  CUDA_ERROR( cudaMallocHost(&(image->Y),
			     cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT]) );
  memset(image->Y, 0,Y_size);
*/
  len += fread(image->Y, 1, width*height, file);

 
  //image->U = (uint8_t*)calloc(1, cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT]);
/*
  CUDA_ERROR( cudaMallocHost(&(image->U),
			     cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT]) );
  memset(image->U, 0, U_size);
*/
  len += fread(image->U, 1, (width*height)/4, file);

  
  //image->V = (uint8_t*)calloc(1, cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT]);
/*
  CUDA_ERROR( cudaMallocHost(&(image->V),
			     cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT]) );
  memset(image->V, 0, V_size);
*/
  len += fread(image->V, 1, (width*height)/4, file);
   
  if (ferror(file))
  {
    transferData td;
    td.type = END; td.totalSize= sizeof(transferData);
    SISCI_ERROR(sendData((uint8_t*)&td,td.totalSize,masterCli));
    perror("ferror");
    exit(EXIT_FAILURE);
  } 
  
  if (feof(file))
  {
    transferData td;
    td.type = END; td.totalSize= sizeof(transferData);
    SISCI_ERROR(sendData((uint8_t*)&td,td.totalSize,masterCli));
    
    free_yuv(image);

    return false;
  }
  else if (len != width*height*1.5)
  {
    
    transferData td;
    td.type = END; td.totalSize= sizeof(transferData);
    SISCI_ERROR(sendData((uint8_t*)&td,td.totalSize,masterCli));

    fprintf(stderr, "Reached end of file, but incorrect bytes read.\n");
    fprintf(stderr, "Wrong input? (height: %d width: %d)\n", height, width);
    
    free_yuv(image);
    
    return false;
  }

  return true;
}

/* Recive yuv from slave node 
Recive volatile memory from reciveData, it is assigned, and cast to non volatile,to image, Y,U,V.
Tested: NO
Bugs:
comments:
*/
bool read_yuv_slave(struct c63_common *cm,int frameNo,serverParams *slaveServ, int *topRow, int *nRows)
{
 
  yuv_t *image = cm->curframe->orig;
  size_t size_Y,size_U,size_V,totalSize,offset;
  volatile uint8_t* memory; //continues memory for siscilib
  transferData *td;
 
  SISCI_ERROR(reciveData(&memory,/*totalSize*/ 0,slaveServ)); //totalSize er unødvendig
  
  td = (transferData*)memory; 
#if RYDEBUG
  fprintf(stderr,"read_yuv_slave: transferData\n");
  fprintf(stderr,"type:      %d\n",td->type);
  fprintf(stderr,"totalSize: %d\n",td->totalSize);
  fprintf(stderr,"size_Y:    %d\n",td->size_Y);
  fprintf(stderr,"size_U:    %d\n",td->size_U);
  fprintf(stderr,"size_V:    %d\n",td->size_V);
  fprintf(stderr,"width:     %d\n",td->width);
  fprintf(stderr,"height:    %d\n",td->height);
  fprintf(stderr,"frameNo:   %d\n",td->frameNo);
#endif
  
  if(td->type == END) {return false;}
  
  totalSize = td->totalSize;
  size_Y = td->size_Y; size_U = td->size_U; size_V = td->size_V; 
  *topRow = td->topRow;
  *nRows  = td->nRows;
  
  /*
  image =(yuv_t *)malloc(sizeof(*image));
  
  CUDA_ERROR(cudaMallocHost(&(image->Y),size_Y));
  CUDA_ERROR(cudaMallocHost(&(image->U),size_U));
  CUDA_ERROR(cudaMallocHost(&(image->V),size_V));
  */

  offset = sizeof(transferData);
  memcpy(image->Y,(const void*)(memory+offset),size_Y); offset+=size_Y;
  memcpy(image->U,(const void*)(memory+offset),size_U); offset+=size_U;
  memcpy(image->V,(const void*)(memory+offset),size_V); offset+=size_V;
						       

  //READ R må også lages
  if (cm->frames_since_keyframe != cm->keyframe_interval && cm->framenum != 0){
#define TEST_TRANSFER_RECONS 1
#if TEST_TRANSFER_RECONS
    if (td->r_size_Y != getBufSize(cm, recons, Y_COMPONENT) ||
        td->r_size_U != getBufSize(cm, recons, U_COMPONENT) ||
        td->r_size_V != getBufSize(cm, recons, V_COMPONENT)){
      fprintf(stderr, "Mismatching bufsizes!\n");
    }
    if (cm->curframe == NULL){ fprintf(stderr, "cm has no curframe!\n"); }
#endif
#undef TEST_TRANSFER_RECONS
    yuv_t *recM = cm->curframe->reconsMerge; // the retrieved reconstructed frame;
    memcpy(recM->Y, (const void *)(memory+offset), td->r_size_Y); offset += td->r_size_Y;
    memcpy(recM->U, (const void *)(memory+offset), td->r_size_U); offset += td->r_size_U;
    memcpy(recM->V, (const void *)(memory+offset), td->r_size_V); offset += td->r_size_V;
  }

#if RYDEBUG
  transferTest tT;
  memcpy(&tT,(const void*)(memory+offset),sizeof(transferTest));
  fprintf(stderr,"TRANSFERTEST RY: %d %d %d\n",tT.test[0],tT.test[1],tT.test[2]);
#endif

  return true;
}



/* Read planar YUV frames with 4:2:0 chroma sub-sampling */
bool read_yuv(FILE *file, struct c63_common *cm)
{
  size_t len = 0;
  //yuv_t *image =(yuv_t *)malloc(sizeof(*image));
  yuv_t *image = cm->curframe->orig;

  //fprintf(stderr,"read_yuv2\n");
  /* Read Y. The size of Y is the same as the size of the image. The indices
     represents the color component (0 is Y, 1 is U, and 2 is V) */
  //image->Y = (uint8_t*)calloc(1, cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT]);
  /*
  CUDA_ERROR( cudaMallocHost(&(image->Y),
      cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT]) );
  memset(image->Y, 0, cm->padw[Y_COMPONENT]*cm->padh[Y_COMPONENT]);
  */
  len += fread(image->Y, 1, width*height, file);

  /* Read U. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y
     because (height/2)*(width/2) = (height*width)/4. */
  //image->U = (uint8_t*)calloc(1, cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT]);
  /*
  CUDA_ERROR( cudaMallocHost(&(image->U),
      cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT]) );
  memset(image->U, 0, cm->padw[U_COMPONENT]*cm->padh[U_COMPONENT]);
  */
  len += fread(image->U, 1, (width*height)/4, file);

  /* Read V. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y. */
  //image->V = (uint8_t*)calloc(1, cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT]);
  /*
  CUDA_ERROR( cudaMallocHost(&(image->V),
      cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT]) );
  memset(image->V, 0, cm->padw[V_COMPONENT]*cm->padh[V_COMPONENT]);
  */
  len += fread(image->V, 1, (width*height)/4, file);

  if (ferror(file))
  {
    perror("ferror");
    exit(EXIT_FAILURE);
  }

  if (feof(file))
  {
    /*
    free(image->Y);
    free(image->U);
    free(image->V);
    free(image);
    */
    free_yuv(image);

    return false;
  }
  else if (len != width*height*1.5)
  {
    fprintf(stderr, "Reached end of file, but incorrect bytes read.\n");
    fprintf(stderr, "Wrong input? (height: %d width: %d)\n", height, width);

    /*
    free(image->Y);
    free(image->U);
    free(image->V);
    free(image);
    */
    free_yuv(image);

    return false;
  }

  return true;
}
