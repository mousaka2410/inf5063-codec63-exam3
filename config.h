#ifndef CONFIG_H_
#define CONFIG_H_

#define dct_1d_sse              dct_1d
#define idct_1d_sse             idct_1d
#define transpose_block         transpose_block_old
#define scale_block             scale_block_old
#define quantize_block          quantize_block_old
#define dequantize_block        dequantize_block
#define dct_quant_block_8x8     dct_quant_block_8x8
#define dequant_idct_block_8x8  dequant_idct_block_8x8
#define sad_block_8x8_sse       sad_block_8x8

#endif /* CONFIG_H_ */
