__global__ void cuda_sad_block_8x8_o(uint8_t *block1, uint8_t *block2,int stride,int top,int bottom,int left,int right ,struct sad *cuda_min_sad)
{

  int numberOfThreads = blockDim.x;
  int tid = blockIdx.x*blockDim.x + threadIdx.x; //tid: threads id

  __shared__ uint8_t shared_block1[64];
  __shared__ extern struct sad shared_sad_min[];
  //extern __shared__ uint8_t shared_block2[];

  int u,v,r,j; //u,v and r loop vars, r is result
  int x_dim, y_dim, xy_dim;
  int x,y,x_ub,y_ub; /*x,y are the biasd x and y, the same as x, and y in the for loops in the
		      orignal me_block_8x8. x_ub,y_ub are  unbiased.
		     x = top + x_ub  y = left + y_ub
		     x_ub = tid%xy_dim  y_ub = tid/xy_dim
		     */

  x_dim = right - left; y_dim = bottom - top;
  xy_dim = x_dim*y_dim;
  x_ub = tid%xy_dim; y_ub = tid/xy_dim;
  x = top + x_ub; y = left = y_ub;

  if(tid <64) {

    int x = tid/8; int z = tid%8;
    shared_block1[tid] = block1[x*stride + z];

  }

  __syncthreads();




  r =0;
  for (v = 0; v < 8; ++v)
    {
      for (u = 0; u < 8; ++u)
        {
        /*y*stride + x  commes from me_block, it's added to the ref value
        before each call on sad_block. stride her is w in me_block*/
            r += abs(block2[v*stride+u + y*stride + x] - block1[v*stride+u]);
        }
    }


  /*
****MIN REDUCE****
*/

  shared_sad_min[tid].sad = r;
  shared_sad_min[tid].x = x; shared_sad_min[tid].y = y;


 __syncthreads();
  for(j = numberOfThreads/2; j != 0; j/=2) {
    if(tid<j) {
      if(shared_sad_min[tid].sad > shared_sad_min[tid+j].sad) {

	shared_sad_min[tid].sad = shared_sad_min[tid+j].sad;
	shared_sad_min[tid].x = shared_sad_min[tid+j].x;
	shared_sad_min[tid].y = shared_sad_min[tid+j].y;
      }

    }
    __syncthreads();

  }

  //printf("tid: %d  min red sad: %d\n",tid,shared_sad_min[tid].sad);
  if(tid == 0) {
    cuda_min_sad->sad = shared_sad_min[0].sad; //ineffektivt, behold på kortet
  }

}

void dct_quant_block_8x8_n(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  float mb_t[8*8];
  float *mb_c;
  float *mb2_c;
  dim3 threads = dim3(8,8);

  int i, v;


  CUDA_ERROR(cudaMalloc((void **)&mb_c,64*sizeof(float)));
  CUDA_ERROR(cudaMalloc((void **)&mb2_c,64*sizeof(float)));

  //cast int16_t => float
  for (i = 0; i < 64; ++i) { mb2[i] = in_data[i];

  }

  /*
  CUDA_ERROR( cudaMemcpy(mb2_c,&mb2[0],64*sizeof(float),cudaMemcpyHostToDevice));
  cuda_dct_1d_test<<<1,threads>>>(mb2_c,mb_c);
  CUDA_ERROR(cudaDeviceSynchronize());
  CUDA_ERROR(cudaMemcpy(&mb_t[0],mb_c,64*sizeof(float),cudaMemcpyDeviceToHost));
  */

  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }

#define DCT_TEST 0
#if DCT_TEST
  for(i =0; i < 64; i++) {
    if(mb[i] != mb_t[i]) {
      printf("mb[%d]:%f  mb_t[%d]:%f\n",i,mb[i],i,mb_t[i]);
    }
  }
#endif
#undef DCT_TEST
  /*
  CUDA_ERROR( cudaMemcpy(mb_c,&mb[0],64*sizeof(float),cudaMemcpyHostToDevice));
  cuda_transpose_block2<<<1,threads>>>(mb_c,mb2_c);
  CUDA_ERROR(cudaDeviceSynchronize());
  CUDA_ERROR(cudaMemcpy(&mb_t[0],mb2_c,64*sizeof(float),cudaMemcpyDeviceToHost));
  */
  transpose_block(mb, mb2);


  #define TRANSPOSE_TEST 0
  #if TRANSPOSE_TEST

  for(i=0; i<64; i++) {
  if(mb2[i] != mb_t[i]) {
      printf("mb[%d]:%f  mb_t[%d]:%f\n",i,mb2[i],i,mb_t[i]); }
  }
  #endif
  #undef TRANSPOSE_TEST

  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);


  //CUDA_ERROR( cudaMemcpy(mb2_c,&mb2[0],64*sizeof(float),cudaMemcpyHostToDevice));
  //cuda_scale_block<<<1,threads>>>(mb2_c,mb_c);
  //CUDA_ERROR(cudaDeviceSynchronize());
  //CUDA_ERROR(cudaMemcpy(&mb_t[0],mb_c,64*sizeof(float),cudaMemcpyDeviceToHost));

  scale_block(mb2, mb);

#define S_TEST 0
#if S_TEST

  for(i=0; i<64; i++) {
  if(mb[i] != mb_t[i]) {
      printf("mb[%d]:%f  mb_t[%d]:%f\n",i,mb[i],i,mb_t[i]); }
  }
#endif
#undef S_TEST

  CUDA_ERROR( cudaMemcpy(mb_c,&mb[0],64*sizeof(float),cudaMemcpyHostToDevice));
  cuda_quantize_block<<<1,threads>>>(mb_c,mb2_c,quant_tbl);
  CUDA_ERROR(cudaDeviceSynchronize());
  CUDA_ERROR(cudaMemcpy(&mb_t[0],mb2_c,64*sizeof(float),cudaMemcpyDeviceToHost));

  //test_tabels<<<1,1>>>();
  //CUDA_ERROR(cudaDeviceSynchronize());

  quantize_block(mb, mb2, quant_tbl);

#define Q_TEST 0
#if Q_TEST

  for(i=0; i<64; i++) {
    if(mb2[i] != mb_t[i]) {
      printf("mb2[%d]:%f  mb_t[%d]:%f\n",i,mb2[i],i,mb_t[i]); }
  }
#endif
#undef Q_TEST


  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }
  CUDA_ERROR(cudaFree(mb_c));
  CUDA_ERROR(cudaFree(mb2_c));
}

void dct_quant_block_8x8_d(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float *mb_cuda;
  float *mb2_cuda;
  float mb[8*8];
  float mb2[8*8];
  float mb_test[8*8]  __attribute((aligned(16)));


  int i, v;

  dim3 blocks = dim3(1,1);
  dim3 threads = dim3(8,8);

  CUDA_ERROR( cudaMalloc(&mb_cuda, 64*sizeof(float)) );
  CUDA_ERROR( cudaMalloc(&mb2_cuda, 64*sizeof(float)) );
  //printf("indata %f\n",in_data[0]);

  for (i = 0; i < 64; ++i) {
    //printf("indata[%d] %d\n",i,in_data[i]);

    mb2[i] = in_data[i]; }

  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb_test+v*8);
    CUDA_ERROR( cudaMemcpy(mb2_cuda,&mb[0],64*sizeof(float),cudaMemcpyHostToDevice)  );

    cuda_dct_1d_test<<<blocks,threads>>>(mb2_cuda,mb_cuda);
    CUDA_ERROR( cudaMemcpy(&mb[0], mb_cuda,8*sizeof(float),
			   			   cudaMemcpyDeviceToHost) );
    /*
    int test;
    for(test =0 ; test<8;test++) {
      if(mb[test] != mb_test[test+v*8])
	printf("mb %f mb_test %f\n",mb[test],mb_test[test+v*8]);
    }
			   */
  }
  //test
  /*for(v = 0; v < 64; ++v) {
    if(mb[v] != mb_test[v])
    printf("mb: %f  mb_test: %f\n",mb[v],mb_test[v]);
    }*/
  transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);

  scale_block(mb2, mb);
  quantize_block(mb, mb2, quant_tbl);

  CUDA_ERROR(cudaFree(mb_cuda));
  CUDA_ERROR(cudaFree(mb2_cuda));

  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }
}

__global__ void test_tabels(void) {
  int i,j;

  printf("****CUDA_TABEL dev_dctlookup****\n");
  for(i =0; i<8;i++) {
    for(j =0;j<8;j++) {
      printf(" %f",dev_dctlookup[j][i]);
    }
    printf("\n");

  }

  printf("****CUDA_TABEL zigzag_U****\n");
  for(i =0; i<64;i++) {

      printf(" %f",dev_zigzag_U[i]);


  }

  printf("****CUDA_TABEL dev_zigzag_V****\n");
  for(i =0; i<64;i++) {

    printf(" %f",dev_zigzag_V[i]);

  }
}


/*Tested: YES
Bugs: yes
Note:  Assumes 8x8 threads
*/
static __global__ void cuda_dct_1d_o(float *in_data, float *out_data)
{
    int i = threadIdx.x;
    int j = threadIdx.y;
    __shared__ float m[8][8];
    m[j][i] = in_data[j] * dev_dctlookup[j][i];
    __syncthreads();

    // Reduce each column
    int n = 8/2;
    while (n > 0) {
        if (i < n) {
            m[j][i] += m[j][i+n];
        }
        __syncthreads();
    }

    if (j == 0){
        out_data[i] = m[0][i];
    }
}




/*
Tested:  NO
Bugs: ?
Comments: -må være 64 threads per block, kan være mange blocks
*/
__global__ static void cuda_idtc_id(float *in_data, float *out_data,float **dctlookup) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int j = blockIdx.y * blockDim.y + threadIdx.y;
  int tid = threadIdx.x + blockDim.x*threadIdx.y;
  


  __shared__ float idct[64];
  
  idct[tid] =  in_data[threadIdx.x]*dctlookup[threadIdx.y][threadIdx.x];
  
  //add reduce on 8 
  __syncthreads();
  if(tid%8 <4) {
    idct[tid] += idct[tid+4];
  }
    __syncthreads();
  if(tid%8<2)
    idct[tid] += idct[tid+2];
  __syncthreads();
  if(tid%8 == 0) {
    idct[tid] += idct[tid+1];
    out_data[tid/8] = idct[tid];
  }



}




  /*
  CUDA_ERROR("cuda_sad_block",cudaPointerGetAttributes(&pointerAtt,block1));
  
  switch(pointerAtt.memoryType) {
    
  case cudaMemoryTypeHost:
    printf("memType: host \n");
    break;
  case cudaMemoryTypeDevice:
    printf("memType: device \n");
    break;
  }
  */
  //block1[1] =111; 

  //printf("stride:%d",stride);

  //__shared__ uint8_t shared_block1[64];
  


/* Motion estimation for 8x8 block */
static void me_block_8x8_cu1(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  const int max_threads = MAXTHREADS; /*Maximum number of threads used in one block,
					may be different then MAXTHREDS. */
  int total_threads =(right-left)*(bottom-top); //Total threads used in one kernel launch.
  int total_blocks = 1; //Total blocks used in one kernel launch.
  
  

  struct sad  *min_sad, *cuda_min_sad;
  cudaMalloc((void **)&cuda_min_sad,sizeof(struct sad));//skal være total_block
  min_sad = (struct sad*)malloc(sizeof(struct sad));//skal være total_blocks,

  cuda_sad_block_8x8<<<total_blocks,total_threads,total_threads*sizeof(struct sad)>>>(orig + my*w + mx, ref,w,top,bottom,left,right,cuda_min_sad);
  cudaDeviceSynchronize();
  cudaMemcpy(min_sad,cuda_min_sad,sizeof(struct sad),cudaMemcpyDeviceToHost);
  //printf("min_sad: %d\n",min_sad[0].sad);

  cudaFree(cuda_min_sad);
 
  //printf("min_sad: %d\n",min_sad->sad);
  

  //printf("test: %d\n",(orig + my*w + mx)[1]);

  
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      if(abs(right -left)>32 || abs(bottom-top) >32)
	printf("l-r %d  t-b  %d\n",left-right, top-bottom);
      int sad;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);

      

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }
  
  if(best_sad != min_sad->sad) {
    printf("best_sad: %d  != min_sad: %d\n", best_sad,min_sad->sad);
  }
 
  

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */
  free(min_sad);
  mb->use_mv = 1;
}




/* Motion estimation for 8x8 block */
static void me_block_8x8_c2(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  uint64_t dirty_hack1[8], dirty_hack2[8];
  uint8_t *block1,*block2;
  uint8_t *cuda_block1,*cuda_block2;
  int *cuda_sad;
  cudaMalloc((void **)&cuda_block1,64*sizeof(uint8_t));
  cudaMalloc((void **)&cuda_block2,64*sizeof(uint8_t));
  cudaMalloc((void **)&cuda_sad,sizeof(int));


  block1 = orig + my*w+mx;
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[w])[0];
  dirty_hack1[2] =((uint64_t*)&block1[w*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[w*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[w*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[w*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[w*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[w*7])[0];


  cudaMemcpy(cuda_block1,(uint8_t *)&dirty_hack1[0],64*sizeof(uint8_t),cudaMemcpyHostToDevice);

  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      
      
      block2 = ref + y*w + x; 
      //__builtin_prefetch(ref + (y)*w+x,2);
      dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[w])[0];
      dirty_hack2[2] =((uint64_t*)&block2[w*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[w*3])[0];
      dirty_hack2[4] =((uint64_t*)&block2[w*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[w*5])[0];
      dirty_hack2[6] =((uint64_t*)&block2[w*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[w*7])[0];
      
      cudaMemcpy(cuda_block2,(uint8_t *)&dirty_hack2[0],64*sizeof(uint8_t),cudaMemcpyHostToDevice);

      int sad;
      //printf("kernel\n");
      cuda2_sad_block_8x8<<<1,64>>>(cuda_block1,cuda_block2, cuda_sad);
      //printf("device!\n");
      //cudaDeviceSynchronize();
      
      cudaError_t cudaerr = cudaDeviceSynchronize();
      if (cudaerr != CUDA_SUCCESS) {
	printf("cuda2_sad_block kernel: %s \n",cudaGetErrorString(cudaerr));
	}
      //printf("device2\n");

      cudaMemcpy(&sad,cuda_sad,sizeof(int),cudaMemcpyDeviceToHost);
      /*
      int sad_test;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad_test);
      if(sad != sad_test) {
	printf("sad:%d sad_test:%d\n",sad,sad_test);

      }
      */
      //printf("test\n");
      
      
      /* printf("(%4d,%4d) - %d\n", x, y, sad); */

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }
  
  cudaFree(cuda_block1);
  cudaFree(cuda_block2);
  cudaFree(cuda_sad);

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}




/* Motion estimation for 8x8 block
  Uses cuda for me
Tested: Yes, not working
Bugs: Gir feil best sad 
*/
static void me_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;
  /*
    First thing to do is to glue the memory together.

   */
  int size_horizontal = abs(left-right), size_vertical = abs(top-bottom);
  int sizeof_t2 = (64+8*size_horizontal)*size_vertical;
  uint8_t t[64];
  uint8_t *t_cuda;
  cudaMalloc((void **)&t_cuda,64*sizeof(uint8_t));
  uint8_t *t2 = (uint8_t*)malloc(sizeof_t2*sizeof(uint8_t));
  uint8_t *t2_cuda;
  cudaMalloc((void**)&t2_cuda,sizeof_t2*sizeof(uint8_t));
  
  uint8_t *block1 = orig + my*w+mx;
  uint8_t *block2 = ref;
  int mem_offset;
  

  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[w],8);
  memcpy(&t[16],&block1[w*2],8); memcpy(&t[24],&block1[w*3],8);
  memcpy(&t[32],&block1[w*4],8); memcpy(&t[40],&block1[w*5],8);
  memcpy(&t[48],&block1[w*6],8); memcpy(&t[56],&block1[w*7],8);

  
  mem_offset =0;
  /* The array consists of 8 bytes for the first 8x8_block and
   then 1 bytes extra for each other 8x8_block, total 8+size_horizontal.
   First block is [0-7], second is [1-8], third [2-9].. 
  */
  memcpy(t2,block2,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w,8+size_horizontal); mem_offset +=8 + size_horizontal; 
  memcpy(t2+mem_offset,block2+w*2,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*3,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*4,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*5,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*6,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*7,8+size_horizontal);
  
  cudaMemcpy(t_cuda,&t[0],64*sizeof(uint8_t),cudaMemcpyHostToDevice);
  cudaMemcpy(t2_cuda,t2,sizeof_t2*sizeof(uint8_t),cudaMemcpyHostToDevice);

  int threads, blocks,grid_orig;
  int *res,*cuda_res;
  /*using max thread 1024 first
    Husk antall utregninger varierer!  med farge ikke farge?
  cudaOccupancyMaxPotentialBlockSize(??) ? 
  optbv = optimal block size var
*/
  int optbsv_sad_block = 1;//denne skal i egen fil som define istede?
  threads = grid_orig= abs(top-bottom)*abs(right-left);
  res = (int*)malloc(threads*sizeof(int));
  cudaMalloc((void**)&cuda_res,threads*sizeof(int));

  blocks = threads/(MAXTHREADS+1) +1; /*if MAXTHREADS is 1024 this is not necessary, as there is 
				  not more then 32*32=1024 threads*/
  threads/=optbsv_sad_block; blocks*=optbsv_sad_block; /* setting the optimal grid size*/
  
  
  

  cuda_sad_block_8x8<<<blocks,threads>>>(t_cuda, t2_cuda,cuda_res);
  cudaDeviceSynchronize();
  

  cudaMemcpy(res,cuda_res,grid_orig*sizeof(int),cudaMemcpyDeviceToHost);

  /*
  printf("res:");
  for(y =0; y<grid;y++) {
    printf(" %d,",res[y]);

  }
  
  printf("\n");
  */
  for (y = 0; y < blocks; ++y)
    {
      for (x = 0; x < threads; ++x)
	{
	  
	  
	  //critical section
	  //solve by min reduction?
	  if (res[x+y*threads] < best_sad)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad = res[x + y*threads];
	    }
	}
    }
  
  
  int best_sad_test =INT_MAX;


  for (y = top; y < bottom; ++y)
    {
      for (x = left; x < right; ++x)
	{
	  
	  int sad;
	  sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
	  
	  
	  
	  if (sad < best_sad_test)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad_test = sad;
	    }
	}
    }
  
  if(best_sad_test != best_sad) {
    printf("me_block: best_sad_test %d  best_sad_cuda: %d\n",best_sad_test, best_sad);

  }
  
  /*Free memory*/
  free(t2);
  free(res);
  cudaFree(t_cuda);
  cudaFree(t2_cuda);
  cudaFree(cuda_res);

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}





/* Motion estimation for 8x8 block
  Uses cuda for me
Tested: Yes, not working
Bugs: Gir feil best sad 
*/
static void me_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;
  /*
    First thing to do is to glue the memory together.

   */
  int size_horizontal = abs(left-right), size_vertical = abs(top-bottom);
  int sizeof_t2 = (64+8*size_horizontal)*size_vertical;
  uint8_t t[64];
  uint8_t *t_cuda;
  cudaMalloc((void **)&t_cuda,64*sizeof(uint8_t));
  uint8_t *t2 = (uint8_t*)malloc(sizeof_t2*sizeof(uint8_t));
  uint8_t *t2_cuda;
  cudaMalloc((void**)&t2_cuda,sizeof_t2*sizeof(uint8_t));
  
  uint8_t *block1 = orig + my*w+mx;
  uint8_t *block2 = ref;
  int mem_offset;
  

  memcpy(&t[0],&block1[0],8); memcpy(&t[8],&block1[w],8);
  memcpy(&t[16],&block1[w*2],8); memcpy(&t[24],&block1[w*3],8);
  memcpy(&t[32],&block1[w*4],8); memcpy(&t[40],&block1[w*5],8);
  memcpy(&t[48],&block1[w*6],8); memcpy(&t[56],&block1[w*7],8);

  
  mem_offset =0;
  /* The array consists of 8 bytes for the first 8x8_block and
   then 1 bytes extra for each other 8x8_block, total 8+size_horizontal.
   First block is [0-7], second is [1-8], third [2-9].. 
  */
  memcpy(t2,block2,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w,8+size_horizontal); mem_offset +=8 + size_horizontal; 
  memcpy(t2+mem_offset,block2+w*2,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*3,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*4,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*5,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*6,8+size_horizontal); mem_offset +=8 + size_horizontal;
  memcpy(t2+mem_offset,block2+w*7,8+size_horizontal);
  
  cudaMemcpy(t_cuda,&t[0],64*sizeof(uint8_t),cudaMemcpyHostToDevice);
  cudaMemcpy(t2_cuda,t2,sizeof_t2*sizeof(uint8_t),cudaMemcpyHostToDevice);

  int threads, blocks,grid_orig;
  int *res,*cuda_res;
  /*using max thread 1024 first
    Husk antall utregninger varierer!  med farge ikke farge?
  cudaOccupancyMaxPotentialBlockSize(??) ? 
  optbv = optimal block size var
*/
  int optbsv_sad_block = 1;//denne skal i egen fil som define istede?
  threads = grid_orig= abs(top-bottom)*abs(right-left);
  res = (int*)malloc(threads*sizeof(int));
  cudaMalloc((void**)&cuda_res,threads*sizeof(int));

  blocks = threads/(MAXTHREADS+1) +1; /*if MAXTHREADS is 1024 this is not necessary, as there is 
				  not more then 32*32=1024 threads*/
  threads/=optbsv_sad_block; blocks*=optbsv_sad_block; /* setting the optimal grid size*/
  
  
  

  cuda_sad_block_8x8<<<blocks,threads>>>(t_cuda, t2_cuda,cuda_res);
  cudaDeviceSynchronize();
  

  cudaMemcpy(res,cuda_res,grid_orig*sizeof(int),cudaMemcpyDeviceToHost);

  /*
  printf("res:");
  for(y =0; y<grid;y++) {
    printf(" %d,",res[y]);

  }
  
  printf("\n");
  */
  for (y = 0; y < blocks; ++y)
    {
      for (x = 0; x < threads; ++x)
	{
	  
	  
	  //critical section
	  //solve by min reduction?
	  if (res[x+y*threads] < best_sad)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad = res[x + y*threads];
	    }
	}
    }
  
  
  int best_sad_test =INT_MAX;


  for (y = top; y < bottom; ++y)
    {
      for (x = left; x < right; ++x)
	{
	  
	  int sad;
	  sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);
	  
	  
	  
	  if (sad < best_sad_test)
	    {
	      mb->mv_x = x - mx;
	      mb->mv_y = y - my;
	      best_sad_test = sad;
	    }
	}
    }
  
  if(best_sad_test != best_sad) {
    printf("me_block: best_sad_test %d  best_sad_cuda: %d\n",best_sad_test, best_sad);

  }
  
  /*Free memory*/
  free(t2);
  free(res);
  cudaFree(t_cuda);
  cudaFree(t2_cuda);
  cudaFree(cuda_res);

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}
