
//stander includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdint.h>

#include <cuda.h>
#include <cuda_runtime.h>
#include "stdcuda.h"
#include "debug.h"

#define SLDEBUG 0


#include "siscilib.h" //for structs, and sisci includes


static unsigned int localAdapterNo;


sci_error_t sisciInit(unsigned int lAdN,unsigned int *localNodeId) {
  
  sci_error_t error;

  localAdapterNo = lAdN;
  SCIInitialize(NO_FLAGS, &error);
  if(error != SCI_ERR_OK) {return error;}

  SCIGetLocalNodeId(localAdapterNo,
                      localNodeId,
                      NO_FLAGS,
                      &error);
  if(error != SCI_ERR_OK) {return error;}

#if SLDEBUG
  printf("localAdapterNo: %u \n",localAdapterNo);
  printf("localNodeId: %u\n",*localNodeId);
#endif

  return SCI_ERR_OK;

}

sci_error_t makeServerSegment(serverParams *server) {

  sci_error_t           error;
  //volatile unsigned int *localMapAddr;
  unsigned int interruptNo = WRITEDONE;
  unsigned int offset = 0;


  SCIOpen(&(server->sd),NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {
    fprintf(stderr,"SCIOpen\n");
    return error;}


  SCICreateSegment(server->sd,&(server->localSegment),
		   server->localSegmentId, server->segmentSize, 
		   NO_CALLBACK, NULL, NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {
    switch(error) {
    case SCI_ERR_SEGMENTID_USED:
      fprintf(stderr,"SCICreateSegment: SCI_ERR_SEGMENTID_USED\n");
      break;
    case SCI_ERR_SIZE_ALIGNMENT:
       fprintf(stderr,"SCICreateSegment:  SCI_ERR_SIZE_ALIGNMENT\n");
       break;
    case  SCI_ERR_SYSTEM:
      fprintf(stderr,"SCICreateSegment: SCI_ERR_SYSTEM\n");
      break;
    default:
      fprintf(stderr,"SCICreateSegment: \n");

    }
    
    return error;}

  SCIPrepareSegment(server->localSegment,localAdapterNo,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {
    fprintf(stderr,"SCIPrepareSegment\n");
    return error;}

  server->localMapAddr = (volatile uint8_t *)SCIMapLocalSegment(server->localSegment,&(server->localMap), 
					    offset,server->segmentSize, NULL,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  SCISetSegmentAvailable(server->localSegment, localAdapterNo, NO_FLAGS, &error);
  if(error != SCI_ERR_OK) {return error;}
  /*
  SCICreateInterrupt(server->sd,&(server->localInterrupt),localAdapterNo,
		     &(server->interruptNo),NULL,
		     NULL,SCI_FLAG_FIXED_INTNO,&error);
  if(error != SCI_ERR_OK) {return error;}
  */
  return SCI_ERR_OK;

}

sci_error_t removeServer(serverParams *server) {
  sci_error_t error;

  SCIUnmapSegment(server->localMap,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  SCIRemoveSegment(server->localSegment,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  /*
  do { 
    SCIRemoveInterrupt(server->localInterrupt,NO_FLAGS,&error);
  } while ( error == SCI_ERR_BUSY  );
  // eksempel fra dolfin bruker sleep og paranoia-- ...
  */

  SCIClose(server->sd,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  return SCI_ERR_OK;
}

sci_error_t makeClientSegment(clientParams *client) {

  sci_error_t           error;
  //volatile unsigned int *remoteMapAddr;
  unsigned int interruptNo =  WRITEDONE;
  
  SCIOpen(&(client->sd),NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}



  do {
    SCIConnectSegment(client->sd,&(client->remoteSegment),client->remoteNodeId,
		      client->remoteSegmentId,localAdapterNo,
		      NO_CALLBACK,NULL,SCI_INFINITE_TIMEOUT,NO_FLAGS,&error);
#if SLDEBUG
    switch(error) {
    case SCI_ERR_OK:
	fprintf(stderr,"SCIConnectSegment: SCI_ERR_OK \n");
	break;
    case SCI_ERR_NO_SUCH_SEGMENT:
      fprintf(stderr,"SCIConnectSegment: SCI_ERR_NO_SUCH_SEGMENT\n");
      break;
    case SCI_ERR_CONNECTION_REFUSED: 
      fprintf(stderr,"SCIConnectSegment: SCI_ERR_CONNECTION_REFUSED \n");
      break;
    case SCI_ERR_TIMEOUT :
      fprintf(stderr,"SCIConnectSegment: SCI_ERR_TIMEOUT \n");
      break;
    case SCI_ERR_NO_LINK_ACCESS:
	fprintf(stderr,"SCIConnectSegment: SCI_ERR_NO_LINK_ACCESS\n");
	break;
    case SCI_ERR_NO_REMOTE_LINK_ACCESS :
      fprintf(stderr,"SCIConnectSegment: SCI_ERR_NO_REMOTE_LINK_ACCESS \n");
      break;
    case SCI_ERR_SYSTEM :
      fprintf(stderr,"SCIConnectSegment: SCI_ERR_SYSTEM \n");
      break;
    default:
        fprintf(stderr,"SCIConnectSegment: ?? \n");
    }
#endif


  } while (error != SCI_ERR_OK);

  client->remoteMapAddr = (volatile uint8_t *)SCIMapRemoteSegment(client->remoteSegment,&(client->remoteMap),0,
				      client->segmentSize,NULL,NO_FLAGS,&error);
#if SLDEBUG
  switch(error) {
    
  case SCI_ERR_OK:
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_OK \n");
    break;
  case SCI_ERR_NOT_CONNECTED:
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_NOT_CONNECTED\n");
    break;
  case SCI_ERR_OUT_OF_RANGE :
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_OUT_OF_RANGE \n");
    break;
  case SCI_ERR_SIZE_ALIGNMENT:
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_SIZE_ALIGNMENT\n");
    break;
  case SCI_ERR_OFFSET_ALIGNMENT:
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_OFFSET_ALIGNMENT\n");
    break;
  case SCI_ERR_BUSY:
    fprintf(stderr,"SCIMapRemoteSegment: SCI_ERR_BUSY \n");
    break;
  default:
    fprintf(stderr,"SCIMapRemoteSegment: x%x\n",error);
  }

#endif

  if(error != SCI_ERR_OK) {return error;}
  /*
   do {
     SCIConnectInterrupt(client->sd,&(client->remoteInterrupt),
			 client->remoteNodeId,localAdapterNo,
			 client->interruptNo,
			 SCI_INFINITE_TIMEOUT,NO_FLAGS,&error);
#if SLDEBUG
	
	switch(error) {
	case SCI_ERR_OK:
	  fprintf(stderr,"SCIConnectInterrupt:  SCI_ERR_OK \n");
	  break;
	  //case SCI_ERR_NO_SUCH_INTNO:
	  //fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_NO_SUCH_INTNO  \n");
	  //break; finnes ikke?
	case SCI_ERR_CONNECTION_REFUSED:
	  fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_CONNECTION_REFUSED  \n");
	  break;
	case SCI_ERR_TIMEOUT:
	  fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_TIMEOUT\n");
	  break;
	default:
	  fprintf(stderr,"SCIConnectInterrupt: %d \n",error);
	  
	}
#endif
  	

    } while (error != SCI_ERR_OK);
  */

  return SCI_ERR_OK;
}
sci_error_t removeClient(clientParams *client) {
  
  sci_error_t error;
 

  SCIUnmapSegment(client->remoteMap,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  SCIDisconnectSegment(client->remoteSegment,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  //SCIDisconnectInterrupt(client->remoteInterrupt,NO_FLAGS,&error);

  SCIClose(client->sd,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  return SCI_ERR_OK;
}


sci_error_t sendData(uint8_t *data,unsigned int size,clientParams *client) {

  
  sci_error_t             error;
  volatile uint8_t        *remoteBuffer;
  sci_sequence_t          sequence;
  sci_sequence_status_t   sequenceStatus;
  int                     i;
  
  if(size >= DEFAULTSEGMENTSIZE) {
    fprintf(stderr,"SENDDATAERROR: senddata is to large, %d bytes, for segmentsize %d\n",
	    size,DEFAULTSEGMENTSIZE);
  }

  SCICreateMapSequence(client->remoteMap,&sequence,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}

  do {
    sequenceStatus = SCIStartSequence(sequence,NO_FLAGS,&error);
    } while (sequenceStatus != SCI_SEQ_OK); 
  
  remoteBuffer = (volatile uint8_t *)(client->remoteMapAddr);
  //for(i =0; i<size;i++) {remoteBuffer[i] = data[i];}
  
  SCIMemCpy(sequence,(void*)(&data[0]), client->remoteMap,0,size,
	     SCI_FLAG_ERROR_CHECK,&error);

   if (error != SCI_ERR_OK) {
     switch(error) {
       
     case SCI_ERR_OUT_OF_RANGE :
       fprintf(stderr,"SCIMemCpy: SCI_ERR_OUT_OF_RANGE \n");
       break;
     case SCI_ERR_SIZE_ALIGNMENT :
       fprintf(stderr,"SCIMemCpy: SCI_ERR_SIZE_ALIGNMENT \n");
       break;
     case SCI_ERR_OFFSET_ALIGNMENT :
       fprintf(stderr,"SCIMemCpy: SCI_ERR_OFFSET_ALIGNMENT \n");
       break;
     case SCI_ERR_TRANSFER_FAILED :
       fprintf(stderr,"SCIMemCpy: SCI_ERR_TRANSFER_FAILED \n");
       break;
     default:
       fprintf(stderr,"SCIMemCpy: x%x\n",error);
     }
   }

#if SLDEBUG
  volatile transferData *td = (volatile transferData *)remoteBuffer;
  fprintf(stderr,"sendData: transferData\n");
  fprintf(stderr,"totalSize: %d\n",td->totalSize);
  fprintf(stderr,"type:      %d\n",td->type);
  fprintf(stderr,"size_Y:    %d\n",td->size_Y);
  fprintf(stderr,"size_U:    %d\n",td->size_U);
  fprintf(stderr,"size_V:    %d\n",td->size_V);
  fprintf(stderr,"width:     %d\n",td->width);
  fprintf(stderr,"height:    %d\n",td->height);
  fprintf(stderr,"frameNo:   %d\n",td->frameNo);
#endif
  

 if(error != SCI_ERR_OK) {return error;}

   do {
     SCIConnectInterrupt(client->sd,&(client->remoteInterrupt),
			 client->remoteNodeId,localAdapterNo,
			 client->interruptNo,
			 SCI_INFINITE_TIMEOUT,NO_FLAGS,&error);
#if SLDEBUG
	
	switch(error) {
	case SCI_ERR_OK:
	  fprintf(stderr,"SCIConnectInterrupt:  SCI_ERR_OK \n");
	  break;
	  //case SCI_ERR_NO_SUCH_INTNO:
	  //fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_NO_SUCH_INTNO  \n");
	  //break; finnes ikke?
	case SCI_ERR_CONNECTION_REFUSED:
	  fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_CONNECTION_REFUSED  \n");
	  break;
	case SCI_ERR_TIMEOUT:
	  fprintf(stderr,"SCIConnectInterrupt: SCI_ERR_TIMEOUT\n");
	  break;
	default:
	  fprintf(stderr,"SCIConnectInterrupt: %d \n",error);
	  
	}
#endif

   } while (error != SCI_ERR_OK);



#if SLDEBUG
  fprintf(stderr,"SCITriggerInterrupt: Triggering intererrupt!\n");
#endif

  SCITriggerInterrupt(client->remoteInterrupt,NO_FLAGS,&error);
  if(error != SCI_ERR_OK) {return error;}



  sequenceStatus = SCICheckSequence(sequence,NO_FLAGS,&error);
  if (sequenceStatus != SCI_SEQ_OK) {
    return SCI_ERR_TRANSFER_FAILED;
  }  
  
   SCIRemoveSequence(sequence,NO_FLAGS, &error);
   if( error != SCI_ERR_OK) {return error;}

   SCIDisconnectInterrupt(client->remoteInterrupt,NO_FLAGS,&error);

   return SCI_ERR_OK;
}

sci_error_t reciveData(volatile uint8_t **data,unsigned int fjern,serverParams *server) {
  
  sci_error_t error;
  int i;
  volatile uint8_t *localMapAddr = (volatile uint8_t*)(server->localMapAddr);
  *data = (volatile uint8_t *)(server->localMapAddr);
  transferData *td;
  unsigned int totalSize;

  SCICreateInterrupt(server->sd,&(server->localInterrupt),localAdapterNo,
		     &(server->interruptNo),NULL,
		     NULL,SCI_FLAG_FIXED_INTNO,&error);

 

#if SLDEBUG
  fprintf(stderr,"waiting for interrupt.. \n");
#endif

  do {
    SCIWaitForInterrupt(server->localInterrupt,SCI_INFINITE_TIMEOUT,NO_FLAGS,&error);
    if (error == SCI_ERR_OK) break;
#if SLDEBUG
    
    switch(error) {
  case SCI_ERR_TIMEOUT:
    fprintf(stderr," SCIWaitForInterrupt: SCI_ERR_TIMEOUT\n");
    break;
  case SCI_ERR_CANCELLED:
    fprintf(stderr,"SCIWaitForInterrupt: SCI_ERR_CANCELLED\n");
    break;
  default:
    fprintf(stderr,"SCIWaitForInterrupt: x%x\n",error);
    }

#endif
    } while (1);

#if SLDEBUG

  fprintf(stderr,"SCIWaitForInterrupt: Interrupt recieved!\n");
#endif 
  /*
  SCIMemCpy(sequence,(void*)(&data[0]), server->remoteMap,0,10*sizeof(int),
	     SCI_FLAG_BLOCK_READ,&error);
  */
 

  td = (transferData *)*data;
  totalSize = td->totalSize;
  
  #if SLDEBUG
  fprintf(stderr,"sendData: transferData\n");
  fprintf(stderr,"totalSize: %d\n",td->totalSize);
  fprintf(stderr,"type:      %d\n",td->type);
  fprintf(stderr,"size_Y:    %d\n",td->size_Y);
  fprintf(stderr,"size_U:    %d\n",td->size_U);
  fprintf(stderr,"size_V:    %d\n",td->size_V);
  fprintf(stderr,"width:     %d\n",td->width);
  fprintf(stderr,"height:    %d\n",td->height);
  fprintf(stderr,"frameNo:   %d\n",td->frameNo);
#endif

  //data = (uint8_t *)localMapAddr;

  //CUDA_ERROR(cudaMallocHost(&data,totalSize));
  //for(i =0; i<totalSize; i++) {data[i] = localMapAddr[i];}
  //memcpy((void *)data,(const void *)localMapAddr,totalSize);
  /*
  SCIMemCpy(sequence,(void*)(&data[0]), server->remoteMap,0,totalSize,
	     SCI_FLAG_BLOCK_READ,&error);
  
  */
  do { 
     SCIRemoveInterrupt(server->localInterrupt,NO_FLAGS,&error);
  } while ( error == SCI_ERR_BUSY  );
  // eksempel fra dolfin bruker sleep og paranoia-- ...


  return SCI_ERR_OK; 
}

sci_error_t sisciExit() {
  
     sci_error_t error;

     SCITerminate();
     return SCI_ERR_OK;
}


void sisciError(sci_error_t error,const char *errFile,int lineNr) {
  //empty!
  if(error != SCI_ERR_OK) {
    fprintf(stderr,"Error  0x%x  in file %s on line %d\n",error,errFile,lineNr);
  }
}

/*

int main(int argc,char *argv[]) {
  //husk og sette disse rikktig!
  
  unsigned int localNodeId;
  
  //unsigned int localSegmentId =666;
  //unsigned int remoteSegmentId =666;
  //unsigned int remoteNodeId =4;
  //size_t segmentSize = 8192; 
  //sci_desc_t sd;
  //sci_local_segment_t localSegment;
  //sci_remote_segment_t remoteSegment;
  //sci_map_t remoteMap;
  //sci_map_t localMap;
  //sci_local_interrupt_t localInterrupt;
  //sci_remote_interrupt_t remoteInterrupt;
  //volatile unsigned int *remoteMapAddr;
  //volatile unsigned int *localMapAddr;
  
  
  serverParams server1;
  clientParams client1;
  
  server1.interruptNo = WRITEDONE; server1.offset =0;
  server1.localSegmentId = 666; server1.remoteNodeId = 4;
  server1.segmentSize = 8192; 
  client1.interruptNo = WRITEDONE; 
  client1.remoteSegmentId = 666; client1.remoteNodeId=8;
  client1.segmentSize = 8192;

  serverParams server2;
  clientParams client2;

  server2.interruptNo = WRITEDONE; server2.offset =0;
  server2.localSegmentId = 66; server2.remoteNodeId = 8;
  server2.segmentSize = 8192; 
  client2.interruptNo = WRITEDONE; 
  client2.remoteSegmentId = 66; client2.remoteNodeId=4;
  client2.segmentSize = 8192;


  ERROR(sisciInit(0,&localNodeId));

  server1.localNodeId = client1.localNodeId = localNodeId;
  server2.localNodeId = client2.localNodeId = localNodeId;

  if(argc == 2) {
     int i,j; 
  SISCI_ERROR(makeServerSegment(&server1));
  SISCI_ERROR(makeClientSegment(&client2));
  unsigned int recive_buffer[10];
  for(j=0;j<200;j++) {
  //printf("server j%d\n",j);
  SISCI_ERROR(reciveData((uint8_t *)&recive_buffer[0],10*sizeof(int),&server1)); 
  
     printf("recive_buffer: ");
     for(i =0; i<10;i++) {
     printf("%d ",recive_buffer[i]);
   }
     printf("\n");
     //SleepMilliseconds(10);
     
     SISCI_ERROR(sendData((uint8_t *)&recive_buffer[0],10*sizeof(int),&client2));
     }
  
  SISCI_ERROR(removeServer(&server1));
  SISCI_ERROR(removeClient(&client2));
  
  }else {
  
    SISCI_ERROR(makeClientSegment(&client1));
    SISCI_ERROR(makeServerSegment(&server2));

    unsigned int send_buffer[10] = {1,2,3,4,5,6,7,8,9,10};
    int i,j;
    for(j=0;j<200;j++) {
    // printf("client j%d/n",j);
     SISCI_ERROR(sendData((uint8_t *)&send_buffer[0],10*sizeof(int),&client1));
     unsigned int recive_buffer[10];
     //SleepMilliseconds(10);
     SISCI_ERROR(reciveData((uint8_t *)&send_buffer[0],10*sizeof(int),&server2)); 
     printf("recive_buffer: ");
     for(i =0; i<10;i++) {
     printf("%d ",send_buffer[i]);
     send_buffer[i]+=1;
     }
     printf("\n");
     
   }
     SISCI_ERROR(removeClient(&client1));
     SISCI_ERROR(removeServer(&server2));

  }
  SISCI_ERROR(sisciExit());
  
  return 0;
}
*/
