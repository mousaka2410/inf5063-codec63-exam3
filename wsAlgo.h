
#ifndef WSALGO
#define WSALGO

unsigned long long gettime();
unsigned long long get_dt(unsigned long long *prev_time);
void wsDiffTime();
void wsStartTime();
int wsScheduleAlgo();

#endif
