#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h> //for gettime


#include "c63.h"
#include "c63_write.h"
#include "common.h"
#include "me.h"
#include "tables.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>


#include "stdcuda.h"
#include "siscilib.h"
#include "read_yuv.h"
#include "wsAlgo.h"

typedef enum {MASTER,SLAVE,SINGEL} nodeType; // SINGEL, none sisci mode

static char *output_file, *input_file;
FILE        *outfile;

static int limit_numframes = 0;

static uint32_t width; 
static uint32_t height;

/* getopt */
extern int  optind;
extern char *optarg;

unsigned int localNodeId; //Id of the sisci adapter, is 0.
unsigned int remoteNodeId; //Id of remote Node, is 4 or 8
nodeType     node = SINGEL; //Type of sisci node,master or slave.
serverParams masterServer;
clientParams masterClient;
serverParams slaveServer;
clientParams slaveClient;

typedef struct transferTest transferTest;
struct transferTest {
  int test[5];
};




#define WWDEBUG 0
static void writeWrapper(struct c63_common *cm, int topRow, int nRows) {
  transferData *td;
  unsigned int offset;
  unsigned int totalSize;
  uint8_t *memory;
  volatile uint8_t *data;

  unsigned int r_size_Y; 
  unsigned int r_size_U; 
  unsigned int r_size_V;

  unsigned int q_size_Y;
  unsigned int q_size_U;
  unsigned int q_size_V;

  unsigned int mv_size_Y;
  unsigned int mv_size_U;
  unsigned int mv_size_V;

  yuv_t recon;
  dct_t resid;
  macroblock *mbsY;
  macroblock *mbsU;
  macroblock *mbsV;
  int nCols;

  int td_endRow;
  int UVtopRow, UVnRows, UVendRow; // corresponding row measures for chroma
  yuv_t *recM; // pointer to cm->curframe->reconsMerge
  unsigned int mb_offset;

  switch(node) {
    
  case SINGEL:
    write_frame(cm);
    break;
      
  case MASTER: 
    //Master recives: MV,Q,Recons
    
    SISCI_ERROR(reciveData(&data,0,&masterServer));
    td = (transferData *)data;
    if(td->frameNo != cm->framenum) {
      fprintf(stderr,"writeWrapper Master: wrong frame recived (r:%d,cm:%d)\n",
	      td->frameNo,cm->framenum);}
    /*
      Integrer inn data fra slave her
     */   

    // find the pointers in the buffer
    offset = sizeof(transferData);

    recon.Y = (uint8_t*) (data + offset); offset += td->r_size_Y;
    recon.U = (uint8_t*) (data + offset); offset += td->r_size_U;
    recon.V = (uint8_t*) (data + offset); offset += td->r_size_V;

    resid.Ydct = (int16_t*) (data + offset); offset += td->q_size_Y;
    resid.Udct = (int16_t*) (data + offset); offset += td->q_size_U;
    resid.Vdct = (int16_t*) (data + offset); offset += td->q_size_V;

    mbsY = (macroblock *) (data+offset); offset += td->mv_size_Y;
    mbsU = (macroblock *) (data+offset); offset += td->mv_size_U;
    mbsV = (macroblock *) (data+offset); offset += td->mv_size_V;

    // copy the data into cm->curframe
    struct frame f;
    f.recons    = &recon;
    f.residuals = &resid;
    recM = cm->curframe->reconsMerge;

    td_endRow = td->topRow + td->nRows;
    UVtopRow = (td->topRow + 1)/2;
    UVendRow = (td_endRow + 1)/2;
    UVnRows  = UVendRow - UVtopRow;
    /*
    copyRows(cm, cm->curframe, &f, recons, Y_COMPONENT, td->topRow, td->nRows);
    copyRows(cm, cm->curframe, &f, recons, U_COMPONENT, UVtopRow, UVnRows);
    copyRows(cm, cm->curframe, &f, recons, V_COMPONENT, UVtopRow, UVnRows); */
    copy_yuv_cc_rows(cm, recM, &recon, Y_COMPONENT, td->topRow, td->nRows);
    copy_yuv_cc_rows(cm, recM, &recon, U_COMPONENT, UVtopRow, UVnRows);
    copy_yuv_cc_rows(cm, recM, &recon, V_COMPONENT, UVtopRow, UVnRows);

    copyRows(cm, cm->curframe, &f, residuals, Y_COMPONENT, td->topRow, td->nRows);
    copyRows(cm, cm->curframe, &f, residuals, U_COMPONENT, UVtopRow, UVnRows);
    copyRows(cm, cm->curframe, &f, residuals, V_COMPONENT, UVtopRow, UVnRows);

    nCols = cm->mb_cols;
    mb_offset = nCols * td->topRow;
    memcpy(cm->curframe->mbs[Y_COMPONENT] + mb_offset, mbsY + mb_offset, nCols*td->nRows*sizeof(struct macroblock));
    nCols /= 2;
    mb_offset = nCols * UVtopRow;
    memcpy(cm->curframe->mbs[U_COMPONENT] + mb_offset, mbsU + mb_offset, nCols*UVnRows*sizeof(struct macroblock));
    memcpy(cm->curframe->mbs[V_COMPONENT] + mb_offset, mbsV + mb_offset, nCols*UVnRows*sizeof(struct macroblock));

    
#if WWDEBUG
  fprintf(stderr,"writeWrapper: Master\n");
  fprintf(stderr,"frameNo:     %d\n",td->frameNo);
  fprintf(stderr,"topRow:      %d\n",td->topRow);
  fprintf(stderr,"nRows:       %d\n",td->nRows);
  fprintf(stderr,"totalSize:   %d\n",td->totalSize);
  fprintf(stderr,"r_size_Y:    %d\n",td->r_size_Y);
  fprintf(stderr,"r_size_U:    %d\n",td->r_size_U);
  fprintf(stderr,"r_size_V:    %d\n",td->r_size_V);
  fprintf(stderr,"mv_size_Y:   %d\n",td->mv_size_Y);
  fprintf(stderr,"mv_size_U:   %d\n",td->mv_size_U);
  fprintf(stderr,"mv_size_V:   %d\n",td->mv_size_V);
  fprintf(stderr,"q_size_Y:    %d\n",td->q_size_Y);
  fprintf(stderr,"q_size_U:    %d\n",td->q_size_U);
  fprintf(stderr,"q_size_V:    %d\n",td->q_size_V);
#endif

#if WWDEBUG
  transferTest tT2;
  fprintf(stderr, "offset: %d, totalSize: %d\n", offset, td->totalSize);
  memcpy(&tT2,(const void*)(memory+offset),sizeof(transferTest));
  fprintf(stderr,"TRANSFERTEST WW: %d %d %d\n",tT2.test[0],tT2.test[1],tT2.test[2]);
#endif

  //write data to cm
  
    write_frame(cm);
    break;
    
  case SLAVE:
    //send data to master
    totalSize = sizeof(transferData);
    
    r_size_Y = getBufSize(cm,recons,Y_COMPONENT);
    r_size_U = getBufSize(cm,recons,U_COMPONENT);
    r_size_V = getBufSize(cm,recons,V_COMPONENT);
    q_size_Y = getBufSize(cm,residuals,Y_COMPONENT);
    q_size_U = getBufSize(cm,residuals,U_COMPONENT);
    q_size_V = getBufSize(cm,residuals,V_COMPONENT);
    mv_size_Y = getMvSize(cm,Y_COMPONENT);
    mv_size_V = getMvSize(cm,V_COMPONENT);
    mv_size_U = getMvSize(cm,U_COMPONENT);
    
    totalSize+=r_size_Y; totalSize+=r_size_U; totalSize+=r_size_V; 
    totalSize+=q_size_Y; totalSize+=q_size_U; totalSize+=q_size_V; 
    totalSize+= mv_size_Y; totalSize+=mv_size_U; totalSize+=mv_size_V;
   
#if WWDEBUG
  totalSize +=sizeof(transferTest);
#endif

    //add more to size to transfer data
    memory =(uint8_t*)malloc(totalSize);
    td = (transferData*)memory;
    td->totalSize = totalSize;
    td->frameNo = cm->framenum; td->type = DATA; 
    td->r_size_Y =r_size_Y; td->r_size_U = r_size_U; td->r_size_V = r_size_V;
    td->q_size_Y =  q_size_Y; td->q_size_U = q_size_U; td->q_size_V = q_size_V;
    td->mv_size_Y = mv_size_Y; td->mv_size_U = mv_size_U; td->mv_size_V = mv_size_V;
    td->topRow = topRow;
    td->nRows  = nRows;

    //memcpy in data
    

    //ER DETTE RIKKTIG
    offset = sizeof(transferData);
    memcpy(memory+offset,cm->curframe->recons->Y,r_size_Y); offset+=r_size_Y; 
    memcpy(memory+offset,cm->curframe->recons->U,r_size_U); offset+=r_size_U;
    memcpy(memory+offset,cm->curframe->recons->V,r_size_V); offset+=r_size_V;

    memcpy(memory+offset,cm->curframe->residuals->Ydct,q_size_Y); offset+=q_size_Y;
    memcpy(memory+offset,cm->curframe->residuals->Udct,q_size_U); offset+=q_size_U;
    memcpy(memory+offset,cm->curframe->residuals->Vdct,q_size_V); offset+=q_size_V;
    
    //memcpy mv
    memcpy(memory+offset,cm->curframe->mbs[Y_COMPONENT],mv_size_Y); offset+=mv_size_Y;
    memcpy(memory+offset,cm->curframe->mbs[U_COMPONENT],mv_size_U); offset+=mv_size_U;
    memcpy(memory+offset,cm->curframe->mbs[V_COMPONENT],mv_size_V);


#if WWDEBUG
  fprintf(stderr,"writeWrapper: Slave\n");
  fprintf(stderr,"frameNo:     %d\n",td->frameNo);
  fprintf(stderr,"topRow:      %d\n",td->topRow);
  fprintf(stderr,"nRows:       %d\n",td->nRows);
  fprintf(stderr,"totalSize:   %d\n",td->totalSize);
  fprintf(stderr,"r_size_Y:    %d\n",td->r_size_Y);
  fprintf(stderr,"r_size_U:    %d\n",td->r_size_U);
  fprintf(stderr,"r_size_V:    %d\n",td->r_size_V);
  fprintf(stderr,"mv_size_Y:   %d\n",td->mv_size_Y);
  fprintf(stderr,"mv_size_U:   %d\n",td->mv_size_U);
  fprintf(stderr,"mv_size_V:   %d\n",td->mv_size_V);
  fprintf(stderr,"q_size_Y:    %d\n",td->q_size_Y);
  fprintf(stderr,"q_size_U:    %d\n",td->q_size_U);
  fprintf(stderr,"q_size_V:    %d\n",td->q_size_V);
 
#endif
#if WWDEBUG
  offset+=mv_size_V;
  transferTest tT1;
  tT1.test[0] =4; tT1.test[1] =5; tT1.test[2] =6;
  memcpy(memory+offset,&tT1,sizeof(transferTest));
#endif

    SISCI_ERROR(sendData(memory,totalSize,&slaveClient));
    free(memory);
    break;
  }
  
}
#undef WWDEBUG

static void c63_encode_buf(struct c63_common *cm, Buftype buftype,
    int color_component, int topRow, int nRows, bool gpu);

static void c63_encode_image(struct c63_common *cm, int topRow, int nRows)
{
  /* Advance to next frame */
  /*
  destroy_frame(cm->refframe);
  cm->refframe = cm->curframe;
  cm->curframe = create_frame(cm, image);

  destroy_frame(cm->dev_refframe, true);
  cm->dev_refframe = cm->dev_curframe;
  cm->dev_curframe = create_frame(cm, NULL, true);
  */

  int endRow = topRow + nRows;
  int UVtopRow = (topRow + 1)/2;
  int UVendRow = (endRow + 1)/2;
  int UVnRows  = UVendRow - UVtopRow;

  /* Check if keyframe */
  if (cm->framenum == 0 || cm->frames_since_keyframe == cm->keyframe_interval)
  {
    cm->curframe->keyframe = 1;
    cm->frames_since_keyframe = 0;

    fprintf(stderr, " (keyframe) ");
  }
  else { cm->curframe->keyframe = 0; }

  copyBuf(cm, cm->dev_curframe, cm->curframe, orig, Y_COMPONENT,
      cudaMemcpyHostToDevice);
  copyBuf(cm, cm->dev_curframe, cm->curframe, orig, U_COMPONENT,
        cudaMemcpyHostToDevice);
  copyBuf(cm, cm->dev_curframe, cm->curframe, orig, V_COMPONENT,
        cudaMemcpyHostToDevice);


  if (!cm->curframe->keyframe)
  {
    /* Motion Estimation */

    if (node == SLAVE){
      // Warning: requires that topRow and nRows is the same as prev. pass!
      yuv_t *rec  = cm->curframe->recons;
      yuv_t *recM = cm->curframe->reconsMerge;
      copy_yuv_cc_rows(cm, recM, rec, Y_COMPONENT, topRow, nRows);
      copy_yuv_cc_rows(cm, recM, rec, U_COMPONENT, UVtopRow, UVnRows);
      copy_yuv_cc_rows(cm, recM, rec, V_COMPONENT, UVtopRow, UVnRows);
    }

    if (node != SINGEL){
      swap_yuv(&(cm->curframe->recons), &(cm->curframe->reconsMerge));
    }

    copyBuf(cm, cm->dev_curframe, cm->curframe, recons, Y_COMPONENT,
        cudaMemcpyDeviceToHost);
    copyBuf(cm, cm->dev_curframe, cm->curframe, recons, U_COMPONENT,
        cudaMemcpyDeviceToHost);

    /*
    copyBuf(cm, cm->dev_refframe, cm->refframe, recons, V_COMPONENT,
        cudaMemcpyHostToDevice);
    */

    struct mv_buf_cut mvY = {0};
    struct mv_buf_cut mvU = {0};
    struct mv_buf_cut mvV = {0};

    // todo: use cuda streams
    c63_start_partial_motion_estimate(cm, Y_COMPONENT, topRow, nRows, true, &mvY);
    c63_start_partial_motion_estimate(cm, U_COMPONENT, UVtopRow, UVnRows, true, &mvU);
    c63_start_partial_motion_estimate(cm, V_COMPONENT, UVtopRow, UVnRows, false, &mvV);

    c63_finalize_partial_motion_estimate(cm, &mvV);// obs: V first
    c63_finalize_partial_motion_estimate(cm, &mvY);
    c63_finalize_partial_motion_estimate(cm, &mvU);


    /*
    copyBuf(cm, cm->refframe, cm->dev_refframe, recons, Y_COMPONENT,
        cudaMemcpyDeviceToHost);
    copyBuf(cm, cm->refframe, cm->dev_refframe, recons, U_COMPONENT,
        cudaMemcpyDeviceToHost);
    copyBuf(cm, cm->refframe, cm->dev_refframe, recons, V_COMPONENT,
        cudaMemcpyDeviceToHost);
    */

    /* Motion Compensation */

    c63_partial_motion_compensate(cm, Y_COMPONENT, topRow, nRows, false);
    c63_partial_motion_compensate(cm, U_COMPONENT, UVtopRow, UVnRows, false);
    c63_partial_motion_compensate(cm, V_COMPONENT, UVtopRow, UVnRows, false);

    copyBuf(cm, cm->dev_curframe, cm->curframe, predicted, Y_COMPONENT,
        cudaMemcpyHostToDevice);
    copyBuf(cm, cm->dev_curframe, cm->curframe, predicted, U_COMPONENT,
        cudaMemcpyHostToDevice);
    /*
    copyBuf(cm, cm->dev_curframe, cm->curframe, predicted, V_COMPONENT,
        cudaMemcpyHostToDevice);
    */
  } else {
    CUDA_ERROR( cudaMemset(cm->dev_curframe->predicted->Y, 0, getBufSize(cm, orig, Y_COMPONENT)) );
    CUDA_ERROR( cudaMemset(cm->dev_curframe->predicted->U, 0, getBufSize(cm, orig, U_COMPONENT)) );
    //CUDA_ERROR( cudaMemset(cm->dev_curframe->predicted->V, 0, getBufSize(cm, orig, V_COMPONENT)) );
    memset(cm->curframe->predicted->V, 0, getBufSize(cm, orig, V_COMPONENT));
  }

  /* DCT and Quantization */

  partial_dct_quantize(cm, Y_COMPONENT, topRow, nRows, true);
  partial_dct_quantize(cm, U_COMPONENT, UVtopRow, UVnRows, true);
  partial_dct_quantize(cm, V_COMPONENT, UVtopRow, UVnRows, false);

  copyBuf(cm, cm->curframe, cm->dev_curframe, residuals, Y_COMPONENT,
      cudaMemcpyDeviceToHost);
  copyBuf(cm, cm->curframe, cm->dev_curframe, residuals, U_COMPONENT,
      cudaMemcpyDeviceToHost);
  /*
  copyBuf(cm, cm->curframe, cm->dev_curframe, residuals, V_COMPONENT,
      cudaMemcpyDeviceToHost);
  */

#define Q_TEST 0
#if Q_TEST
  int eps = 0;
  int16_t *out_data_Y;
  size_t curr_size = cm->ypw * cm->yph;
  out_data_Y = (int16_t*) malloc(curr_size * sizeof(int16_t));
  dct_quantize(image->Y, cm->curframe->predicted->Y, cm->padw[Y_COMPONENT],
  		cm->padh[Y_COMPONENT], out_data_Y, cm->quanttbl[Y_COMPONENT]);
#if Q_TEST == 1
  for (size_t i=0; i<curr_size; ++i){
  	int diff = cm->curframe->residuals->Ydct[i] - out_data_Y[i];
  	if (abs(diff) > eps){
  		printf("dq %d: %d ~ %d\n", (int) i, (int) out_data_Y[i],
  				(int) cm->curframe->residuals->Ydct[i]);
  	}
  }
#endif /* Q_TEST == 1 */
  free(out_data_Y);
#endif /* Q_TEST */
#undef Q_TEST

  /* Reconstruct frame for inter-prediction */

  partial_dequantize_idct(cm, Y_COMPONENT, topRow, nRows, true);
  partial_dequantize_idct(cm, U_COMPONENT, UVtopRow, UVnRows, true);
  partial_dequantize_idct(cm, V_COMPONENT, UVtopRow, UVnRows, false);

  copyBuf(cm, cm->curframe, cm->dev_curframe, recons, Y_COMPONENT,
      cudaMemcpyDeviceToHost);
  copyBuf(cm, cm->curframe, cm->dev_curframe, recons, U_COMPONENT,
      cudaMemcpyDeviceToHost);
  /*
  copyBuf(cm, cm->curframe, cm->dev_curframe, recons, V_COMPONENT,
      cudaMemcpyDeviceToHost);
  */

  /* Function dump_image(), found in common.c, can be used here to check if the
     prediction is correct */
  cudaDeviceSynchronize();

  writeWrapper(cm, topRow, nRows);

  if (node == MASTER){
    // merge recons
    yuv_t *rec  = cm->curframe->recons;
    yuv_t *recM = cm->curframe->reconsMerge;
    copy_yuv_cc_rows(cm, recM, rec, Y_COMPONENT, topRow, nRows);
    copy_yuv_cc_rows(cm, recM, rec, U_COMPONENT, UVtopRow, UVnRows);
    copy_yuv_cc_rows(cm, recM, rec, V_COMPONENT, UVtopRow, UVnRows);
  }

  ++cm->framenum;
  ++cm->frames_since_keyframe;
}

struct c63_common* init_c63_enc(int width, int height)
{
  int i;

  /* calloc() sets allocated memory to zero */
  struct c63_common *cm = (c63_common*)calloc(1, sizeof(struct c63_common));

  cm->width = width;
  cm->height = height;

  cm->padw[Y_COMPONENT] = cm->ypw = (uint32_t)(ceil(width/16.0f)*16);
  cm->padh[Y_COMPONENT] = cm->yph = (uint32_t)(ceil(height/16.0f)*16);
  cm->padw[U_COMPONENT] = cm->upw = (uint32_t)(ceil(width*UX/(YX*8.0f))*8);
  cm->padh[U_COMPONENT] = cm->uph = (uint32_t)(ceil(height*UY/(YY*8.0f))*8);
  cm->padw[V_COMPONENT] = cm->vpw = (uint32_t)(ceil(width*VX/(YX*8.0f))*8);
  cm->padh[V_COMPONENT] = cm->vph = (uint32_t)(ceil(height*VY/(YY*8.0f))*8);

  cm->mb_cols = cm->ypw / 8;
  cm->mb_rows = cm->yph / 8;

  /* Quality parameters -- Home exam deliveries should have original values,
   i.e., quantization factor should be 25, search range should be 16, and the
   keyframe interval should be 100. */
  cm->qp = 25;                  // Constant quantization factor. Range: [1..50]
  cm->me_search_range = 16;     // Pixels in every direction
  cm->keyframe_interval = 100;  // Distance between keyframes

  /* Initialize quantization tables */
  for (i = 0; i < 64; ++i)
  {
    cm->quanttbl[Y_COMPONENT][i] = yquanttbl_def[i] / (cm->qp / 10.0);
    cm->quanttbl[U_COMPONENT][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
    cm->quanttbl[V_COMPONENT][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
  }

  CUDA_ERROR( cudaMalloc(&(cm->dev_quanttbl[Y_COMPONENT]), 64*sizeof(uint8_t)) );
  CUDA_ERROR( cudaMalloc(&(cm->dev_quanttbl[U_COMPONENT]), 64*sizeof(uint8_t)) );
  CUDA_ERROR( cudaMalloc(&(cm->dev_quanttbl[V_COMPONENT]), 64*sizeof(uint8_t)) );

  CUDA_ERROR( cudaMemcpy(cm->dev_quanttbl[Y_COMPONENT], 
              cm->quanttbl[Y_COMPONENT], 64*sizeof(uint8_t),
              cudaMemcpyHostToDevice) );
  CUDA_ERROR( cudaMemcpy(cm->dev_quanttbl[U_COMPONENT],
              cm->quanttbl[U_COMPONENT], 64*sizeof(uint8_t),
              cudaMemcpyHostToDevice) );
  CUDA_ERROR( cudaMemcpy(cm->dev_quanttbl[V_COMPONENT],
              cm->quanttbl[V_COMPONENT], 64*sizeof(uint8_t),
              cudaMemcpyHostToDevice) );
  return cm;
}

static void print_help()
{
  printf("Usage: ./c63enc [options] input_file\n");
  printf("Commandline options:\n");
  printf("  -h                             Height of images to compress\n");
  printf("  -w                             Width of images to compress\n");
  printf("  -o                             Output file (.c63)\n");
  printf("  [-f]                           Limit number of frames to encode\n");
  printf("  -n                             sisci remote node\n");
  printf("  -s                             slave\n");
  printf("  -m                             master\n");
  printf("\n");

  exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
  int c;
  //yuv_t *image;
  yuv_t *image2;
   
  bool gotImage = true;

  if (argc == 1) { print_help(); }

  while ((c = getopt(argc, argv, "h:w:o:f:i:smn:")) != -1)
  {
    switch (c)
      {
      case 'h':
	height = atoi(optarg);
	break;
      case 'w':
	width = atoi(optarg);
	break;
      case 'o':
	output_file = optarg;
	break;
      case 'f':
	limit_numframes = atoi(optarg);
	break;
      case 'n':
	remoteNodeId = atoi(optarg);
	break;
      case 's':
	node = SLAVE;
	break;
      case 'm':
	node = MASTER;
	if(node == SLAVE) {
	  fprintf(stderr,"slave or master?\n"); //gjør noe mer her??
	}
	break;
      default:
	print_help();
	break;
      }
  }
  if(node == SLAVE)
    printf("Slave node, remoteNode: %d\n",remoteNodeId);
  else if(node == MASTER)
    printf("Master node, remoteNode: %d\n",remoteNodeId);

  if (optind >= argc)
  {
    fprintf(stderr, "Error getting program options, try --help.\n");
    exit(EXIT_FAILURE);
  }
  
  outfile = fopen(output_file, "wb");

  if (outfile == NULL)
  {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

#define INITDEBUG 1

  if(node != SINGEL) {
    sisciInit(0,&localNodeId); 
    if(node == MASTER) {
      
     
      
      masterClient.interruptNo = WRITEDONE; masterClient.remoteSegmentId = SLAVESERVERNODEID;
      masterClient.remoteNodeId = remoteNodeId; masterClient.segmentSize = DEFAULTSEGMENTSIZE; 
      masterClient.localNodeId = localNodeId;
      SISCI_ERROR(makeClientSegment(&masterClient));
      fprintf(stderr,"masterClient.segmentSize %d\n",masterClient.segmentSize);

      masterServer.interruptNo = WRITEDONE; masterServer.localSegmentId = MASTERSERVERNODEID;
      masterServer.remoteNodeId = remoteNodeId; masterServer.segmentSize = DEFAULTSEGMENTSIZE; 
      masterServer.localNodeId = localNodeId;
      SISCI_ERROR(makeServerSegment(&masterServer));
      
      //send  INIT frame
      transferData td;
#if INITDEBUG
  fprintf(stderr,"Init: Master\n");
  fprintf(stderr,"width:     %d\n",width);
  fprintf(stderr,"height:    %d\n",height);
#endif

      memset(&td,0,sizeof(transferData));
      td.width = width; td.height = height; td.type = INIT;
      SISCI_ERROR(sendData((uint8_t *)&td,sizeof(transferData),&masterClient));


    }else if (node == SLAVE) {
      
      
      slaveServer.interruptNo = WRITEDONE; slaveServer.localSegmentId = SLAVESERVERNODEID;
      slaveServer.remoteNodeId = remoteNodeId; slaveServer.segmentSize = DEFAULTSEGMENTSIZE; 
      slaveServer.localNodeId = localNodeId;
      SISCI_ERROR(makeServerSegment(&slaveServer));
      fprintf(stderr,"slaveServer.segmnetSize %d\n",slaveServer.segmentSize);

      slaveClient.interruptNo = WRITEDONE; slaveClient.remoteSegmentId = MASTERSERVERNODEID;
      slaveClient.remoteNodeId = remoteNodeId; slaveClient.segmentSize = DEFAULTSEGMENTSIZE; 
      slaveClient.localNodeId = localNodeId;
      SISCI_ERROR(makeClientSegment(&slaveClient));

      
      //recive INIT frame
      volatile uint8_t *data;
      transferData *td;
      SISCI_ERROR(reciveData(&data,0,&slaveServer));
      td = (transferData*)data;
      width = td->width; height = td->height;
#if INITDEBUG
  fprintf(stderr,"Init: Slave\n");
  fprintf(stderr,"width:     %d\n",td->width);
  fprintf(stderr,"height:    %d\n",td->height);
#endif
#undef INITDEBUG      
    }

  }


  struct c63_common *cm = init_c63_enc(width, height);
  cm->e_ctx.fp = outfile;

  input_file = argv[optind];

  if (limit_numframes) { printf("Limited to %d frames.\n", limit_numframes); }

  FILE *infile = fopen(input_file, "rb");

  if (infile == NULL)
  {
    perror("fopen");
    exit(EXIT_FAILURE);
  }

  /* Encode input frames */
  int numframes = 0;
 
    
  tablesToDevice();
  
  unsigned long long curr_time = gettime();
  
  

  init_read_yuv(width,height); 

  yuv_t *orig_buf = (yuv_t *) malloc(sizeof(yuv_t));
  orig_buf->Y = (uint8_t*) alloc_buf(getBufSize(cm, orig, Y_COMPONENT), false);
  orig_buf->U = (uint8_t*) alloc_buf(getBufSize(cm, orig, U_COMPONENT), false);
  orig_buf->V = (uint8_t*) alloc_buf(getBufSize(cm, orig, V_COMPONENT), false);
  cm->curframe =  create_frame(cm, orig_buf, false);
  cm->dev_curframe = create_frame(cm, NULL, true);

  // Hack..
  cm->refframe = cm->curframe;
  cm->dev_refframe = cm->dev_curframe;

  while (1)
  {

    int topRowToSend, nRowsToSend;
    int totalRows = cm->mb_rows;
    int topRow = 0, nRows = totalRows;
    
    switch(node) {
    case SINGEL:
      gotImage = read_yuv(infile, cm);
      break;
    case MASTER:
      //image = read_yuv(infile, cm);
      gotImage = read_yuv_master(infile,cm,numframes,&masterClient);

      topRowToSend = totalRows/2;
      nRowsToSend  = totalRows - topRowToSend;
      topRow = 0;
      nRows  = topRowToSend - topRow;

      if (gotImage){
        send_yuv(cm, cm->curframe->orig, numframes, &masterClient, topRowToSend, nRowsToSend);
      }

      break;
    case SLAVE:
      //image = read_yuv(infile, cm);
      gotImage = read_yuv_slave(cm,numframes,&slaveServer, &topRow, &nRows);
      break;
    }
    if (!gotImage) { break; }

    printf("Encoding frame %d, ", numframes);
    c63_encode_image(cm, topRow, nRows);
    printf("time ms: %llu ", get_dt(&curr_time));

    /*
    cudaFree(image->Y);
    cudaFree(image->U);
    cudaFree(image->V);
    free(image);
    */
    //free_yuv(image);

    printf("Done!\n");

    ++numframes;

    if (limit_numframes && numframes >= limit_numframes) { break; }
  }

  fclose(outfile);
  fclose(infile);

  if(node != SINGEL) {

    if(node == MASTER) {
      SISCI_ERROR(removeClient(&masterClient));
    }else if (node == SLAVE) {
      SISCI_ERROR(removeServer(&slaveServer));
    }
    SISCI_ERROR(sisciExit());
  }


  //int i, j;
  //for (i = 0; i < 2; ++i)
  //{
  //  printf("int freq[] = {");
  //  for (j = 0; j < ARRAY_SIZE(frequencies[i]); ++j)
  //  {
  //    printf("%d, ", frequencies[i][j]);
  //  }
  //  printf("};\n");
  //}

  return EXIT_SUCCESS;
}
