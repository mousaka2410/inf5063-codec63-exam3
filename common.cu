#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "dsp.h"
#include "stdcuda.h"

void dequantize_idct_row(int16_t *in_data, uint8_t *prediction, int w, int h,
    int y, uint8_t *out_data, uint8_t *quantization)
{
  int x;

  int16_t block[8*8];

  /* Perform the dequantization and iDCT */
  for(x = 0; x < w; x += 8)
  {
    int i, j;

    dequant_idct_block_8x8(in_data+(x*8), block, quantization);

    for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
      {
        /* Add prediction block. Note: DCT is not precise -
           Clamp to legal values */
        int16_t tmp = block[i*8+j] + (int16_t)prediction[i*w+j+x];

        if (tmp < 0) { tmp = 0; }
        else if (tmp > 255) { tmp = 255; }

        out_data[i*w+j+x] = tmp;
      }
    }
  }
}

void cuda_dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization)
{
  dim3 blocks = dim3(width/8, height/8); // one for each macro block
  dim3 threads = dim3(8,8); // one for each pixel in macro block
  cuda_dequant_idct_block_8x8<<<blocks,threads>>>(in_data, prediction, out_data, quantization);
}

void dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    dequantize_idct_row(in_data+y*width, prediction+y*width, width, height, y,
        out_data+y*width, quantization);
  }
}

void partial_dequantize_idct(struct c63_common *cm, int color_component,
    int topRow, int nRows, bool gpu){

  int nCols = cm->mb_cols;
  if (color_component != Y_COMPONENT){
    nCols /= 2;
  }

  // Select buffers
  struct frame *curframe = cm->curframe;
  if (gpu){ curframe = cm->dev_curframe; }
  int16_t *quant = select_dct(curframe->residuals, color_component);
  uint8_t *pred  = select_yuv(curframe->predicted, color_component);
  uint8_t *res   = select_yuv(curframe->recons, color_component);

  size_t pad_elements = topRow * getBufRowElemnts(cm, color_component);
  quant += pad_elements;
  pred  += pad_elements;
  res   += pad_elements;

  if (gpu){
    cuda_dequantize_idct(quant, pred, nCols*8, nRows*8, res, cm->dev_quanttbl[color_component]);
  } else {
    dequantize_idct(quant, pred, nCols*8, nRows*8, res, cm->quanttbl[color_component]);
  }
}

void dct_quantize_row(uint8_t *in_data, uint8_t *prediction, int w, int h,
    int16_t *out_data, uint8_t *quantization)
{
  int x;

  int16_t block[8*8];

  /* Perform the DCT and quantization */
  for(x = 0; x < w; x += 8)
  {
    int i, j;

    for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
      {
        block[i*8+j] = ((int16_t)in_data[i*w+j+x] - prediction[i*w+j+x]);
      }
    }

    /* Store MBs linear in memory, i.e. the 64 coefficients are stored
       continuous. This allows us to ignore stride in DCT/iDCT and other
       functions. */
    dct_quant_block_8x8(block, out_data+(x*8), quantization);
  }
}

void cuda_dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization)
{
  dim3 blocks = dim3(width/8, height/8); // one for each macro block
  dim3 threads = dim3(8,8); // one for each pixel in macro block
  cuda_dct_quant_block_8x8<<<blocks,threads>>>(in_data, prediction, out_data, quantization);
}

void dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    dct_quantize_row(in_data+y*width, prediction+y*width, width, height,
        out_data+y*width, quantization);
  }
}

void partial_dct_quantize(struct c63_common *cm, int color_component,
    int topRow, int nRows, bool gpu){

  int nCols = cm->mb_cols;
  if (color_component != Y_COMPONENT){
    nCols /= 2;
  }

  // Select buffers
  struct frame *curframe = cm->curframe;
  if (gpu){ curframe = cm->dev_curframe; }
  uint8_t *orig = select_yuv(curframe->orig, color_component);
  uint8_t *pred = select_yuv(curframe->predicted, color_component);
  int16_t *res  = select_dct(curframe->residuals, color_component);

  size_t pad_elements = topRow * getBufRowElemnts(cm, color_component);
  orig += pad_elements;
  pred += pad_elements;
  res  += pad_elements;

  if (gpu){
    cuda_dct_quantize(orig, pred, nCols*8, nRows*8, res, cm->dev_quanttbl[color_component]);
  } else {
    dct_quantize(orig, pred, nCols*8, nRows*8, res, cm->quanttbl[color_component]);
  }
}

void free_yuv(yuv_t *image, int gpu){
  cudaError_t (*freeFunc)(void *ptr);

  if (gpu){ freeFunc = cudaFree; }
  else    { freeFunc = cudaFreeHost; }

  CUDA_ERROR( freeFunc(image->Y) );
  CUDA_ERROR( freeFunc(image->U) );
  CUDA_ERROR( freeFunc(image->V) );
  free(image);
}

void free_dct(dct_t *image, int gpu){
  cudaError_t (*freeFunc)(void *ptr);

  if (gpu){ freeFunc = cudaFree; }
  else    { freeFunc = cudaFreeHost; }

  CUDA_ERROR( freeFunc(image->Ydct) );
  CUDA_ERROR( freeFunc(image->Udct) );
  CUDA_ERROR( freeFunc(image->Vdct) );
  free(image);
}

void destroy_frame(struct frame *f, bool gpu)
{
  /* First frame doesn't have a reconstructed frame to destroy */
  if (!f) { return; }

  cudaError_t (*freeFunc)(void *ptr);

  if (gpu){ freeFunc = cudaFree; }
  else    { freeFunc = cudaFreeHost; }

  free_yuv(f->recons, gpu);
  free_dct(f->residuals, gpu);
  free_yuv(f->predicted, gpu);
  free_yuv(f->reconsMerge, gpu);

  CUDA_ERROR( freeFunc(f->mbs[Y_COMPONENT]) );
  CUDA_ERROR( freeFunc(f->mbs[U_COMPONENT]) );
  CUDA_ERROR( freeFunc(f->mbs[V_COMPONENT]) );
  
  free(f);
}

void* alloc_buf(size_t size, bool gpu){
  void* res;
  if (gpu){
    CUDA_ERROR( cudaMalloc(&res, size) );
    CUDA_ERROR( cudaMemset(res, 0, size) );
  } else {
    CUDA_ERROR( cudaMallocHost(&res, size) );
    CUDA_ERROR( cudaMemset(res, 0, size) );
  }
  return res;
}

struct frame* create_frame(struct c63_common *cm, yuv_t *image, bool gpu)
{
  struct frame *f = (struct frame *)malloc(sizeof(struct frame));

  f->orig = image;

  if (gpu){
    f->orig = (yuv_t *)malloc(sizeof(yuv_t));
    f->orig->Y = (uint8_t *)alloc_buf(cm->ypw * cm->yph, gpu);
    f->orig->U = (uint8_t *)alloc_buf(cm->upw * cm->uph, gpu);
    f->orig->V = (uint8_t *)alloc_buf(cm->vpw * cm->vph, gpu);
  }

  f->recons = (yuv_t *)malloc(sizeof(yuv_t));
  f->recons->Y = (uint8_t *)alloc_buf(cm->ypw * cm->yph, gpu);
  f->recons->U = (uint8_t *)alloc_buf(cm->upw * cm->uph, gpu);
  f->recons->V = (uint8_t *)alloc_buf(cm->vpw * cm->vph, gpu);

  f->predicted = (yuv_t *)malloc(sizeof(yuv_t));
  f->predicted->Y = (uint8_t*)alloc_buf(cm->ypw * cm->yph * sizeof(uint8_t), gpu);
  f->predicted->U = (uint8_t*)alloc_buf(cm->upw * cm->uph * sizeof(uint8_t), gpu);
  f->predicted->V = (uint8_t*)alloc_buf(cm->vpw * cm->vph * sizeof(uint8_t), gpu);

  f->reconsMerge = (yuv_t *)malloc(sizeof(yuv_t));
  f->reconsMerge->Y = (uint8_t*)alloc_buf(cm->ypw * cm->yph * sizeof(uint8_t), gpu);
  f->reconsMerge->U = (uint8_t*)alloc_buf(cm->upw * cm->uph * sizeof(uint8_t), gpu);
  f->reconsMerge->V = (uint8_t*)alloc_buf(cm->vpw * cm->vph * sizeof(uint8_t), gpu);

  f->residuals = (dct_t*)malloc(sizeof(dct_t));
  f->residuals->Ydct = (int16_t*)alloc_buf(cm->ypw * cm->yph * sizeof(int16_t), gpu);
  f->residuals->Udct = (int16_t*)alloc_buf(cm->upw * cm->uph * sizeof(int16_t), gpu);
  f->residuals->Vdct = (int16_t*)alloc_buf(cm->vpw * cm->vph * sizeof(int16_t), gpu);

  /*
   Used by quantize_block, in dsp.cu    
   */
  if (!gpu){
  f->mbs[Y_COMPONENT] =
      (struct macroblock *)
      alloc_buf(cm->mb_rows * cm->mb_cols * sizeof(struct macroblock), gpu);
  f->mbs[U_COMPONENT] =
      (struct macroblock *)
      alloc_buf(cm->mb_rows/2 * cm->mb_cols/2 * sizeof(struct macroblock), gpu);
  f->mbs[V_COMPONENT] =
      (struct macroblock *)
      alloc_buf(cm->mb_rows/2 * cm->mb_cols/2 * sizeof(struct macroblock), gpu);
  } else {
    f->mbs[Y_COMPONENT] = NULL;
    f->mbs[U_COMPONENT] = NULL;
    f->mbs[V_COMPONENT] = NULL;
  }
  return f;
}

// gets the pointer to a buffer
void* getBuf(struct frame *f, Buftype buftype,
    int color_component)
{
  switch (buftype){
  case orig:      return select_yuv(f->orig, color_component);
  case recons:    return select_yuv(f->recons, color_component);
  case predicted: return select_yuv(f->predicted, color_component);
  case residuals: return select_dct(f->residuals, color_component);
  default: return NULL;
  }
}

// gets the total size of a buffer
size_t getBufSize(struct c63_common *cm, Buftype buftype,
    int color_component){
  size_t size = sizeof(uint8_t);
  if (buftype == residuals) size = sizeof(int16_t);
  size *= cm->padw[color_component] * cm->padh[color_component];
  return size;
}

// gets the size of one row of macro blocks (8 lines)
size_t getBufRowSize(struct c63_common *cm, Buftype buftype,
    int color_component){
  size_t size = sizeof(uint8_t);
  if (buftype == residuals) size = sizeof(int16_t);
  size *= cm->padw[color_component] * 8; // return the size of 8 lines
  return size;
}

size_t getMvSize(struct c63_common *cm, int color_component){
  return cm->padw[color_component]/8 * cm->padh[color_component]/8 * sizeof(struct macroblock);
}

// copies nRows rows of macro blocks.
void copyRows(struct c63_common *cm,
    struct frame *to_f, struct frame *from_f, Buftype buftype,
    int color_component, int topRow, int nRows, enum cudaMemcpyKind kind)
{
  uint8_t *from = (uint8_t*) getBuf(from_f, buftype, color_component);
  uint8_t *to   = (uint8_t*) getBuf(to_f, buftype, color_component);

  size_t rowSize = getBufRowSize(cm, buftype, color_component);
  size_t start = topRow * rowSize;
  size_t size  = nRows  * rowSize;
  CUDA_ERROR( cudaMemcpy(to+start, from+start, size, kind) );
}

void copyBuf(struct c63_common *cm,
    struct frame *to_f, struct frame *from_f, Buftype buftype,
    int color_component, enum cudaMemcpyKind kind)
{
  uint8_t *from = (uint8_t*) getBuf(from_f, buftype, color_component);
  uint8_t *to   = (uint8_t*) getBuf(to_f, buftype, color_component);

  size_t size = getBufSize(cm, buftype, color_component);
  CUDA_ERROR( cudaMemcpy(to, from, size, kind) );
}

void copy_yuv_cc_rows(struct c63_common *cm, struct yuv *to_yuv, struct yuv *from_yuv,
    int color_component, int topRow, int nRows, enum cudaMemcpyKind kind){
  uint8_t *to = select_yuv(to_yuv, color_component);
  uint8_t *from = select_yuv(from_yuv, color_component);
  size_t rowElemnts = getBufRowElemnts(cm, color_component);
  to   += topRow * rowElemnts;
  from += topRow * rowElemnts;
  size_t size = nRows * rowElemnts * sizeof(uint8_t);
  CUDA_ERROR( cudaMemcpy(to, from, size, kind) );
}

void swap_yuv(yuv_t **a, yuv_t **b){
  yuv *tmp = *a;
  *a = *b;
  *b = tmp;
}

void dump_image(yuv_t *image, int w, int h, FILE *fp)
{
  fwrite(image->Y, 1, w*h, fp);
  fwrite(image->U, 1, w*h/4, fp);
  fwrite(image->V, 1, w*h/4, fp);
}
