foreman_4k.yuv, 10 frames

gprof:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls   s/call   s/call  name    
 28.57      4.78     4.78 31104000     0.00     0.00  idct_1d
 27.97      9.46     4.68 31104000     0.00     0.00  dct_1d
  7.17     10.66     1.20  7776000     0.00     0.00  transpose_block
  5.44     11.57     0.91  1944000     0.00     0.00  quantize_block
  4.39     12.31     0.74  1944000     0.00     0.00  dct_quant_block_8x8
  4.30     13.03     0.72  3888000     0.00     0.00  scale_block
  4.21     13.73     0.71  1944000     0.00     0.00  dequant_idct_block_8x8
  3.95     14.39     0.66     5400     0.00     0.00  dequantize_idct_row
  3.83     15.03     0.64  1944000     0.00     0.00  dequantize_block
  3.29     15.58     0.55     5400     0.00     0.00  dct_quantize_row
  2.99     16.08     0.50  1749600     0.00     0.00  mc_block_8x8
  2.27     16.46     0.38  1944000     0.00     0.00  write_block

nvprof:

==26689== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 99.97%  19.8925s        27  736.76ms  20.914ms  2.19311s  cuda_me_block_8x8


output:

Encoding frame 0, time ms: 1874 Done!
Encoding frame 1, time ms: 4173 Done!
Encoding frame 2, time ms: 4139 Done!
Encoding frame 3, time ms: 4141 Done!
Encoding frame 4, time ms: 4142 Done!
Encoding frame 5, time ms: 4143 Done!
Encoding frame 6, time ms: 4143 Done!
Encoding frame 7, time ms: 4143 Done!
Encoding frame 8, time ms: 4144 Done!
Encoding frame 9, time ms: 4144 Done!
(totally ~39 seconds)
