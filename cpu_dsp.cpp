#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

#define NO_CUDA 1
#include "dsp.h"
#include "tables.h"

//own 
#include <mmintrin.h> //MMX
#include <xmmintrin.h> //sse
#include <emmintrin.h> //sse2
#include <tmmintrin.h> //ssse3
#include <immintrin.h> //avx/avx2
#include <string.h> //memcpy
#include <stdio.h>  //printf 

#include "config.h"

/*
SSE version of transpose 1.0
Useses supplied macro _MM_TRANSPOSE4_PS to transpose a 8x8 matrix.
As the matrix to transpose is a 8x8 matrix of floats, and sse only can take 4 floats in a XMM 
register, we have to divede the calculation in 4 parts. 

     * * * * * * * * *    * * * * * * * * *
     * first * second*    *   T   *   T   *
     *  4x4  *  4x4  *    * first * third *
     *       *       *    *  4x4  *  4x4  *
   T(* * * * * * * * *)=  * * * * * * * * *
     * third *fourth *    *   T   *   T   *  
     *  4x4  *  4x4  *    *second *fourth *
     *       *       *    *  4x4  *  4x4  *
     * * * * * * * * *    * * * * * * * * *
Time: Saves 1-2ms on total runtime (messured on the lab machine) 
Bugs:none
tested:ok
*/
static void transpose_block_sse(float *in_data, float *out_data)
{
 
  __m128 row1,row2,row3,row4;
  
 
  //first 4x4
  row1 = _mm_load_ps(&in_data[0]);
  row2 = _mm_load_ps(&in_data[8]);
  row3 = _mm_load_ps(&in_data[16]);
  row4 = _mm_load_ps(&in_data[24]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[0],row1);
  _mm_store_ps(&out_data[8],row2);
  _mm_store_ps(&out_data[16],row3);
  _mm_store_ps(&out_data[24],row4);

  //third 4x4
  row1 = _mm_load_ps(&in_data[32]);
  row2 = _mm_load_ps(&in_data[40]);
  row3 = _mm_load_ps(&in_data[48]);
  row4 = _mm_load_ps(&in_data[56]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[4],row1);
  _mm_store_ps(&out_data[12],row2);
  _mm_store_ps(&out_data[20],row3);
  _mm_store_ps(&out_data[28],row4);

  //second 4x4
  row1 = _mm_load_ps(&in_data[4]);
  row2 = _mm_load_ps(&in_data[12]);
  row3 = _mm_load_ps(&in_data[20]);
  row4 = _mm_load_ps(&in_data[28]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[32],row1);
  _mm_store_ps(&out_data[40],row2);
  _mm_store_ps(&out_data[48],row3);
  _mm_store_ps(&out_data[56],row4);

  
  //fourth 4x4
  row1 = _mm_load_ps(&in_data[36]);
  row2 = _mm_load_ps(&in_data[44]);
  row3 = _mm_load_ps(&in_data[52]);
  row4 = _mm_load_ps(&in_data[60]);
  _MM_TRANSPOSE4_PS(row1, row2, row3, row4);
  _mm_store_ps(&out_data[36],row1);
  _mm_store_ps(&out_data[44],row2);
  _mm_store_ps(&out_data[52],row3);
  _mm_store_ps(&out_data[60],row4);

  /*
  //Test
  int x;
  float test_out[64] __attribute__ ((aligned (16)));
  transpose_block_old(in_data,&test_out[0]);
  for(x =0; x<64;x++) {
    if(test_out[x] != out_data[x]) {
      printf("Bug in Transpose x:%d test_data: %f  out_data: %f \n",x,test_out[x],out_data[x]);
    }
  }
  */
  
}


static void transpose_block_old(float *in_data, float *out_data)
{
  int i, j;

 

  for (i = 0; i < 8; ++i)
  {
    for (j = 0; j < 8; ++j)
    {
      out_data[i*8+j] = in_data[j*8+i];
    }
  }
}



#ifdef __AVX__
// altered dct functions using avx instructions

/*
This function does a vector*matrix multiplication between in_data and the 
dctlookup matrix and stores the result into out_data. Let in_data, the
dctlookup matrix and out_data be partitioned as follows:

                      ************
                      *          *
                      *    A1    *
    ***************   *          *   ***************
    * in1  * in2  * * ************ = *     out     *
    ***************   *          *   ***************
                      *    A2    *
                      *          *
                      ************

Then we see that 

out = in1*A1 + in2*A2 

The out vector can then be accumulated by taking the first element of in1 
multiplied with the first row of A1, added with the second element of in1 
multiplied the second row A1 and so forth, until the last element of in2 
multiplied with the last row A2.

*/
static void dct_1d_avx(float *in_data, float *out_data){
    __m256 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    // load first half of in_data (in1) into both halves of avx-register
    in = _mm256_broadcast_ps((__m128*) in_data);
    // load the upper half of dctlookup (A1) into avx-registers
    lu0 = _mm256_load_ps(dctlookup[0]);
    lu1 = _mm256_load_ps(dctlookup[1]);
    lu2 = _mm256_load_ps(dctlookup[2]);
    lu3 = _mm256_load_ps(dctlookup[3]);

    sum = _mm256_set1_ps(0.0f); 

    // fill the first value of in_data into the whole of bc
    bc = _mm256_permute_ps(in, 0);
    // multiply the value with a row of lu0
    mul = _mm256_mul_ps(bc, lu0);
    // add the product to the result vector
    sum = _mm256_add_ps(mul, sum);

    // second value of in_data
    bc = _mm256_permute_ps(in, 0x55);
    mul = _mm256_mul_ps(bc, lu1);
    sum = _mm256_add_ps(mul, sum);

    // third value of in_data
    bc = _mm256_permute_ps(in, 0xAA);
    mul = _mm256_mul_ps(bc, lu2);
    sum = _mm256_add_ps(mul, sum);

    // fourth value of in_data
    bc = _mm256_permute_ps(in, 0xFF);
    mul = _mm256_mul_ps(bc, lu3);
    sum = _mm256_add_ps(mul, sum);


    // load second half of in_data (in2) into both halves of avx-register
    in = _mm256_broadcast_ps((__m128*)&in_data[4]);
    // load the lower half of dctlookup (A2) into avx-registers
    lu0 = _mm256_load_ps(dctlookup[4]);
    lu1 = _mm256_load_ps(dctlookup[5]);
    lu2 = _mm256_load_ps(dctlookup[6]);
    lu3 = _mm256_load_ps(dctlookup[7]);

    // fifth value of in_data
    bc = _mm256_permute_ps(in, 0);
    mul = _mm256_mul_ps(bc, lu0);
    sum = _mm256_add_ps(mul, sum);

    // sixth value of in_data
    bc = _mm256_permute_ps(in, 0x55);
    mul = _mm256_mul_ps(bc, lu1);
    sum = _mm256_add_ps(mul, sum);

    // seventh value of in_data
    bc = _mm256_permute_ps(in, 0xAA);
    mul = _mm256_mul_ps(bc, lu2);
    sum = _mm256_add_ps(mul, sum);

    // eigth value of in_data
    bc = _mm256_permute_ps(in, 0xFF);
    mul = _mm256_mul_ps(bc, lu3);
    sum = _mm256_add_ps(mul, sum);

    // save the resulting vector into out_data. We're done.
    _mm256_storeu_ps(out_data, sum);

#define TEST_DCT 0
#if TEST_DCT
    float out_d[8]; 
    int x;
    dct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_DCT
}

/*
see dct_1d_avx for more detailed explanation. Only difference is that
dctlookuptr is used instead of dctlookup.
*/
static void idct_1d_avx(float *in_data, float *out_data){
    __m256 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    in = _mm256_broadcast_ps((__m128*) in_data);
    lu0 = _mm256_load_ps(dctlookuptr[0]);
    lu1 = _mm256_load_ps(dctlookuptr[1]);
    lu2 = _mm256_load_ps(dctlookuptr[2]);
    lu3 = _mm256_load_ps(dctlookuptr[3]);

    sum = _mm256_set1_ps(0.0f);

    bc = _mm256_permute_ps(in, 0);
    mul = _mm256_mul_ps(bc, lu0);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0x55);
    mul = _mm256_mul_ps(bc, lu1);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0xAA);
    mul = _mm256_mul_ps(bc, lu2);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0xFF);
    mul = _mm256_mul_ps(bc, lu3);
    sum = _mm256_add_ps(mul, sum);


    in = _mm256_broadcast_ps((__m128*)&in_data[4]);
    lu0 = _mm256_load_ps(dctlookuptr[4]);
    lu1 = _mm256_load_ps(dctlookuptr[5]);
    lu2 = _mm256_load_ps(dctlookuptr[6]);
    lu3 = _mm256_load_ps(dctlookuptr[7]);

    bc = _mm256_permute_ps(in, 0);
    mul = _mm256_mul_ps(bc, lu0);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0x55);
    mul = _mm256_mul_ps(bc, lu1);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0xAA);
    mul = _mm256_mul_ps(bc, lu2);
    sum = _mm256_add_ps(mul, sum);

    bc = _mm256_permute_ps(in, 0xFF);
    mul = _mm256_mul_ps(bc, lu3);
    sum = _mm256_add_ps(mul, sum);

    _mm256_storeu_ps(out_data, sum);

#define TEST_IDCT 0
#if TEST_IDCT
    float out_d[8];
    int x;
    idct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_IDCT
}

#endif /* __AVX__ */


// altered dct functions using sse instructions
/*

This function does a vector*matrix multiplication between in_data and the 
dctlookup matrix and stores the result into out_data. Let in_data, the
dctlookup matrix and out_data be partitioned as follows:

                      ***************
                      *      *      *
                      *  A1  *  A3  *
    ***************   *      *      *   ***************
    * in1  * in2  * * *************** = * out1 * out2 *
    ***************   *      *      *   ***************
                      *  A2  *  A4  *
                      *      *      *
                      ***************

Then we see that

out1 = in1*A1 + in2*A2
out2 = in1*A3 + in2*A4

So out1 and out2 can seperately be accumulated in a similar way as described by
dct_1d_avx.

Parantheses in the comments in the code refers to the above description of the
partitioning.
*/
static void dct_1d_sse(float *in_data, float *out_data)
{
    //unsigned int shuffles[] = {0,0x55,0xAA,0xFF};
    __m128 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    // load first half of in_data (in1) into sse-register
    in  = _mm_load_ps(in_data);
    // load first quadrant of dctlookup (A1) into sse-registers
    lu0 = _mm_load_ps(dctlookup[0]);
    lu1 = _mm_load_ps(dctlookup[1]);
    lu2 = _mm_load_ps(dctlookup[2]);
    lu3 = _mm_load_ps(dctlookup[3]);

    sum = _mm_set_ss(0); // set to 0, better way?

    // fill bc register with first value of in_data (in_data[0])
    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    // second value of in_data
    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    // third value of in_data
    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    // fourth value of in_data
    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // load second half of in_data (in2)
    in  = _mm_load_ps(&in_data[4]);
    // load second quadrant (A2)
    lu0 = _mm_load_ps(dctlookup[4]);
    lu1 = _mm_load_ps(dctlookup[5]);
    lu2 = _mm_load_ps(dctlookup[6]);
    lu3 = _mm_load_ps(dctlookup[7]);

    // sum should remain the same

    // fifth value of in_data
    bc  = _mm_shuffle_ps(in, in, 0); // check if shuffle is correct
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    // sixth value of in_data
    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    // seventh value of in_data
    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    // eigth value of in_data
    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // store result to first half of out_data (out1)
    _mm_store_ps(out_data, sum);

    // Now do the same with A3 and A4 to calculate out2:

    // third quadrant (A3)
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(&dctlookup[0][4]);
    lu1 = _mm_load_ps(&dctlookup[1][4]);
    lu2 = _mm_load_ps(&dctlookup[2][4]);
    lu3 = _mm_load_ps(&dctlookup[3][4]);

    sum = _mm_set_ss(0);

    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // fourth quadrant (A4)
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(&dctlookup[4][4]);
    lu1 = _mm_load_ps(&dctlookup[5][4]);
    lu2 = _mm_load_ps(&dctlookup[6][4]);
    lu3 = _mm_load_ps(&dctlookup[7][4]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // store result to second half of out_data (out2)
    _mm_store_ps(&out_data[4], sum);


    //Test
#define TEST_DCT 0
#if TEST_DCT
    float out_d[8]; 
    int x;
    dct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_DCT
}
   

static void idct_1d_sse(float *in_data, float *out_data)
{

    //unsigned int shuffles[] = {0,0x55,0xAA,0xFF};
    __m128 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    // first quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(dctlookuptr[0]);
    lu1 = _mm_load_ps(dctlookuptr[1]);
    lu2 = _mm_load_ps(dctlookuptr[2]);
    lu3 = _mm_load_ps(dctlookuptr[3]);

    sum = _mm_set_ss(0); // set to 0, better way?

    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // second quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(dctlookuptr[4]);
    lu1 = _mm_load_ps(dctlookuptr[5]);
    lu2 = _mm_load_ps(dctlookuptr[6]);
    lu3 = _mm_load_ps(dctlookuptr[7]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0); // check if shuffle is correct
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    _mm_store_ps(out_data, sum);

    // third quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(&dctlookuptr[0][4]);
    lu1 = _mm_load_ps(&dctlookuptr[1][4]);
    lu2 = _mm_load_ps(&dctlookuptr[2][4]);
    lu3 = _mm_load_ps(&dctlookuptr[3][4]);

    sum = _mm_set_ss(0);

    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    // fourth quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(&dctlookuptr[4][4]);
    lu1 = _mm_load_ps(&dctlookuptr[5][4]);
    lu2 = _mm_load_ps(&dctlookuptr[6][4]);
    lu3 = _mm_load_ps(&dctlookuptr[7][4]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0);
    mul = _mm_mul_ps(bc, lu0);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    mul = _mm_mul_ps(bc, lu1);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    mul = _mm_mul_ps(bc, lu2);
    sum = _mm_add_ps(mul, sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    mul = _mm_mul_ps(bc, lu3);
    sum = _mm_add_ps(mul, sum);

    _mm_store_ps(&out_data[4], sum);


    //Test
#define TEST_IDCT 0
#if TEST_IDCT
    float out_d[8]; 
    int x;
    idct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_IDCT
}

#ifdef __FMA__
// altered dct functions using sse and FMA instructions.
// To see how the function works, see the description of function dct_1d_sse;
// only differense is that this function fuses the multiplication (_mm_mul_ps)
// and addition (_mm_add_ps) into one single instruction (_mm_fmadd_ps)

//from gprof seems to execute a little slower, 0.4(SSE) to 0.5(SSE+FMA).
//from messured execution time, little faster, ~1ms
static void dct_1d_sse_FMA(float *in_data, float *out_data)
{
    //unsigned int shuffles[] = {0,0x55,0xAA,0xFF};
    __m128 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    // first quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(dctlookup[0]);
    lu1 = _mm_load_ps(dctlookup[1]);
    lu2 = _mm_load_ps(dctlookup[2]);
    lu3 = _mm_load_ps(dctlookup[3]);

    sum = _mm_set_ss(0); // set to 0, better way?

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    // second quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(dctlookup[4]);
    lu1 = _mm_load_ps(dctlookup[5]);
    lu2 = _mm_load_ps(dctlookup[6]);
    lu3 = _mm_load_ps(dctlookup[7]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0); // check if shuffle is correct
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    _mm_store_ps(out_data, sum);

    // third quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(&dctlookup[0][4]);
    lu1 = _mm_load_ps(&dctlookup[1][4]);
    lu2 = _mm_load_ps(&dctlookup[2][4]);
    lu3 = _mm_load_ps(&dctlookup[3][4]);

    sum = _mm_set_ss(0);

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    // fourth quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(&dctlookup[4][4]);
    lu1 = _mm_load_ps(&dctlookup[5][4]);
    lu2 = _mm_load_ps(&dctlookup[6][4]);
    lu3 = _mm_load_ps(&dctlookup[7][4]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    _mm_store_ps(&out_data[4], sum);


    //Test
#define TEST_DCT 0
#if TEST_DCT
    float out_d[8]; 
    int x;
    dct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_DCT
}
#endif /* __FMA__ */


#ifdef __FMA__
// See dct_1d_sse for a description of how this function works.
static void idct_1d_sse_FMA(float *in_data, float *out_data)
{
    //unsigned int shuffles[] = {0,0x55,0xAA,0xFF};
    __m128 in, lu0, lu1, lu2, lu3, bc, mul, sum;

    // first quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(dctlookuptr[0]);
    lu1 = _mm_load_ps(dctlookuptr[1]);
    lu2 = _mm_load_ps(dctlookuptr[2]);
    lu3 = _mm_load_ps(dctlookuptr[3]);

    sum = _mm_set_ss(0); // set to 0, better way?

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    // second quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(dctlookuptr[4]);
    lu1 = _mm_load_ps(dctlookuptr[5]);
    lu2 = _mm_load_ps(dctlookuptr[6]);
    lu3 = _mm_load_ps(dctlookuptr[7]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0); // check if shuffle is correct
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    _mm_store_ps(out_data, sum);

    // third quadrant
    in  = _mm_load_ps(in_data);
    lu0 = _mm_load_ps(&dctlookuptr[0][4]);
    lu1 = _mm_load_ps(&dctlookuptr[1][4]);
    lu2 = _mm_load_ps(&dctlookuptr[2][4]);
    lu3 = _mm_load_ps(&dctlookuptr[3][4]);

    sum = _mm_set_ss(0);

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    // fourth quadrant
    in  = _mm_load_ps(&in_data[4]);
    lu0 = _mm_load_ps(&dctlookuptr[4][4]);
    lu1 = _mm_load_ps(&dctlookuptr[5][4]);
    lu2 = _mm_load_ps(&dctlookuptr[6][4]);
    lu3 = _mm_load_ps(&dctlookuptr[7][4]);

    // sum should remain the same

    bc  = _mm_shuffle_ps(in, in, 0);
    sum =_mm_fmadd_ps(bc,lu0,sum);

    bc = _mm_shuffle_ps(in, in, 0x55);
    sum =_mm_fmadd_ps(bc,lu1,sum);

    bc = _mm_shuffle_ps(in, in, 0xAA);
    sum =_mm_fmadd_ps(bc,lu2,sum);

    bc = _mm_shuffle_ps(in, in, 0xFF);
    sum =_mm_fmadd_ps(bc,lu3,sum);

    _mm_store_ps(&out_data[4], sum);


    //Test
#define TEST_IDCT 0
#if TEST_IDCT
    float out_d[8]; 
    int x;
    idct_1d_old(in_data,&out_d[0]);
    float delta_sum = 0;
    for(x =0;x<8;x++) {
        float delta = out_d[x] - out_data[x];
        delta_sum += abs(delta);
        if (delta != 0) printf("%f,",delta);
    }
    if (delta_sum != 0) printf("\n");
#endif
#undef TEST_IDCT
}
#endif /* __FMA__ */

// old functions
static void dct_1d_old(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    float dct = 0;

    for (j = 0; j < 8; ++j)
    {
      dct += in_data[j] * dctlookup[j][i];
    }

    out_data[i] = dct;
  }
}

static void idct_1d_old(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    float idct = 0;

    for (j = 0; j < 8; ++j)
    {
      idct += in_data[j] * dctlookup[i][j];
    }

    out_data[i] = idct;
  }
}


/*SSE version of scalse_block 1.0 V1
Time: saves 1-2 ms on total runtime (messured on the lab machine).
Bugs:None
Tested:OK

Issues:
The order of operations on floats has an effect, in_data[v*8+u] * a1 * a2; gives a bug
but in_data[v*8+u] * (a1 * a2); works fine, whene a1 = ISQRT2 and a2 = ISQRT2 
Test give this (scale_block_sse,vector index, scale_block_old, diff) 
(-5.500000 ,0, -5.500000  0.0000004768)  (-2.500000 ,0, -2.500000  0.0000002384) 
fixed this by only multyplaing with one of the ISQRT in sse, and doing the last one with the
FPU. 
*/
static void scale_block_sse(float *in_data, float *out_data)
{

  //printf("isqrt %f  %f\n",ISQRT2,ISQRT2*ISQRT2);
    //make it static  is slower.
  float firstline[8] __attribute__ ((aligned (16))) ={ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2};
  float line[8] __attribute__ ((aligned (16))) = {ISQRT2,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f};
  
  __m128 line1,line2,line3,line4,line5,line6,line7,line8,line_c1a,line_c2a,line_c1b,line_c2b;


  //four firste lines
  line_c1a = _mm_load_ps(&firstline[0]); //not sure what is fastes, load or set
  //line_c1a = _mm_set_ps1(ISQRT2);
  line_c1b = _mm_load_ps(&firstline[4]);
  //line_c1b = _mm_set_ps1(ISQRT2);

  line_c2a = _mm_load_ps(&line[0]);
  line_c2b = _mm_load_ps(&line[4]);

  line1 = _mm_loadu_ps(&in_data[0]);
  line2 = _mm_loadu_ps(&in_data[4]);
  line3 = _mm_loadu_ps(&in_data[8]);
  line4 = _mm_loadu_ps(&in_data[12]);
  line5 = _mm_loadu_ps(&in_data[16]);
  line6 = _mm_loadu_ps(&in_data[20]);
  line7 = _mm_loadu_ps(&in_data[24]);
  line8 = _mm_loadu_ps(&in_data[28]);

  line1 = _mm_mul_ps (line1,line_c1a);
  line2 = _mm_mul_ps (line2,line_c1b);
  line3 = _mm_mul_ps (line3,line_c2a);
  line4 = _mm_mul_ps (line4,line_c2b);
  line5 = _mm_mul_ps (line5,line_c2a);
  line6 = _mm_mul_ps (line6,line_c2b);
  line7 = _mm_mul_ps (line7,line_c2a);
  line8 = _mm_mul_ps (line8,line_c2b);
	
  _mm_storeu_ps(&out_data[0],line1);
  _mm_storeu_ps(&out_data[4],line2);
  _mm_storeu_ps(&out_data[8],line3);
  _mm_storeu_ps(&out_data[12],line4);
  _mm_storeu_ps(&out_data[16],line5);
  _mm_storeu_ps(&out_data[20],line6);
  _mm_storeu_ps(&out_data[24],line7);
  _mm_storeu_ps(&out_data[28],line8);
  out_data[0]*=ISQRT2; //fixes issue with order of float problem

  //four last lines
  line1 = _mm_loadu_ps(&in_data[32]);
  line2 = _mm_loadu_ps(&in_data[36]);
  line3 = _mm_loadu_ps(&in_data[40]);
  line4 = _mm_loadu_ps(&in_data[44]);
  line5 = _mm_loadu_ps(&in_data[48]);
  line6 = _mm_loadu_ps(&in_data[52]);
  line7 = _mm_loadu_ps(&in_data[56]);
  line8 = _mm_loadu_ps(&in_data[60]);
  
  line1 = _mm_mul_ps (line1,line_c2a);
  line2 = _mm_mul_ps (line2,line_c2b);
  line3 = _mm_mul_ps (line3,line_c2a);
  line4 = _mm_mul_ps (line4,line_c2b);
  line5 = _mm_mul_ps (line5,line_c2a);
  line6 = _mm_mul_ps (line6,line_c2b);
  line7 = _mm_mul_ps (line7,line_c2a);
  line8 = _mm_mul_ps (line8,line_c2b);
	
  _mm_storeu_ps(&out_data[32],line1);
  _mm_storeu_ps(&out_data[36],line2);
  _mm_storeu_ps(&out_data[40],line3);
  _mm_storeu_ps(&out_data[44],line4);
  _mm_storeu_ps(&out_data[48],line5);
  _mm_storeu_ps(&out_data[52],line6);
  _mm_storeu_ps(&out_data[56],line7);
  _mm_storeu_ps(&out_data[60],line8);


 
  /*  
  //Test
   // Må testes mere
  int x;
  float out_d[64];
  float diff;
  scale_block_old(in_data,&out_d[0]);
  //printf("scale sse   (out_data , out)  \n");
  for(x =0; x<64; x++) {
    diff = out_data[x] -out_d[x];
    if(diff >0.0) {
      printf("(%f ,%d, %f  %.10f)  ",out_data[x],x,out_d[x],diff);
    }
  }
  //printf("\n");
  */
}



#ifdef __AVX__
//not tested!!
//gir +10 ms mere fra 81 til 90
static void scale_block_avx(float *in_data, float *out_data)
{

  //printf("isqrt %f  %f\n",ISQRT2,ISQRT2*ISQRT2);
    //make this global, or static
  float firstline[8] __attribute__ ((aligned (32))) ={ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2,ISQRT2};
  float line[8] __attribute__ ((aligned (32))) = {ISQRT2,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f};
  
  __m256 line1,line2,line3,line4,line5,line6,line7,line8,line_c1,line_c2;



  line_c1 = _mm256_loadu_ps(&firstline[0]);
  line_c2 = _mm256_loadu_ps(&line[0]);
  
  line1 = _mm256_loadu_ps(&in_data[0]);
  line2 = _mm256_loadu_ps(&in_data[8]);
  line3 = _mm256_loadu_ps(&in_data[16]);
  line4 = _mm256_loadu_ps(&in_data[24]);
  line5 = _mm256_loadu_ps(&in_data[32]);
  line6 = _mm256_loadu_ps(&in_data[40]);
  line7 = _mm256_loadu_ps(&in_data[48]);
  line8 = _mm256_loadu_ps(&in_data[56]);

  line1 = _mm256_mul_ps (line1,line_c1);
  line2 = _mm256_mul_ps (line2,line_c2);
  line3 = _mm256_mul_ps (line3,line_c2);
  line4 = _mm256_mul_ps (line4,line_c2);
  line5 = _mm256_mul_ps (line5,line_c2);
  line6 = _mm256_mul_ps (line6,line_c2);
  line7 = _mm256_mul_ps (line7,line_c2);
  line8 = _mm256_mul_ps (line8,line_c2);
	

  _mm256_storeu_ps(&out_data[0],line1);
  _mm256_storeu_ps(&out_data[8],line2);
  _mm256_storeu_ps(&out_data[16],line3);
  _mm256_storeu_ps(&out_data[24],line4);
  _mm256_storeu_ps(&out_data[32],line5);
  _mm256_storeu_ps(&out_data[40],line6);
  _mm256_storeu_ps(&out_data[48],line7);
  _mm256_storeu_ps(&out_data[56],line8);
  out_data[0]*=ISQRT2;
 
  /*
  //Test
   // Må testes mere
  int x;
  float out_d[64];
  float diff;
  scale_block_old(in_data,&out_d[0]);
  printf("scale avx   (out_data , out)  \n");
  for(x =0; x<64; x++) {
    diff = out_data[x] -out_d[x];
    if(diff >0.0) {
      printf("(%f ,%d, %f)",out_data[x],x,out_d[x]);
    }
  }
  printf("\n");
  */
}
#endif /* __AVX__ */


static void scale_block_old(float *in_data, float *out_data)
{
  int u, v;

  //printf("isqrt %f  %f\n",ISQRT2,ISQRT2*ISQRT2);

  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
      float a1 = !u ? ISQRT2 : 1.0f;
      float a2 = !v ? ISQRT2 : 1.0f;

      /* Scale according to normalizing function */
      out_data[v*8+u] = in_data[v*8+u] * a1 * a2;
    }
  }
}

//!!!Not tested!!! ca 1ms faster
static void quantize_block_sse(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;
  float dct[64];
  
  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];


    dct[zigzag] = in_data[v*8+u];
  }
  float fours[4] = {4.0f,4.0f,4.0f,4.0f};
  __m128 line1,line2,line3,line4,line5,line6,line7,line8,line_a;
  __m128i char1,char2,char3,char4;
 
  //S1
  line_a = _mm_loadu_ps(&fours[0]);
  line1 = _mm_loadu_ps(&dct[0]);
  line2 = _mm_loadu_ps(&dct[4]);
  line3 = _mm_loadu_ps(&dct[8]);
  line4 = _mm_loadu_ps(&dct[12]);
  
  char1 = _mm_loadu_si128((const __m128i*)&quant_tbl[0]);
  char2 = _mm_loadu_si128((const __m128i*)&quant_tbl[4]);
  char3 = _mm_loadu_si128((const __m128i*)&quant_tbl[8]);
  char4 = _mm_loadu_si128((const __m128i*)&quant_tbl[12]);

  line5 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char1));
  line6 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char2));
  line7 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char3));
  line8 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char4));
 
  line1 = _mm_div_ps (line1,line_a);
  line2 = _mm_div_ps (line2,line_a);
  line3 = _mm_div_ps (line3,line_a);
  line4 = _mm_div_ps (line4,line_a);

  
  line1 = _mm_div_ps (line1,line5);
  line2 = _mm_div_ps (line2,line6);
  line3 = _mm_div_ps (line3,line7);
  line4 = _mm_div_ps (line4,line8);

  line1 = _mm_round_ps(line1,_MM_FROUND_TO_NEAREST_INT);
  line2 = _mm_round_ps(line2,_MM_FROUND_TO_NEAREST_INT);
  line3 = _mm_round_ps(line3,_MM_FROUND_TO_NEAREST_INT);
  line4 = _mm_round_ps(line4,_MM_FROUND_TO_NEAREST_INT);
  	
  _mm_storeu_ps(&out_data[0],line1);
  _mm_storeu_ps(&out_data[4],line2);
  _mm_storeu_ps(&out_data[8],line3);
  _mm_storeu_ps(&out_data[12],line4);
  //E1

    //S2
  line_a = _mm_loadu_ps(&fours[0]);
  line1 = _mm_loadu_ps(&dct[16]);
  line2 = _mm_loadu_ps(&dct[20]);
  line3 = _mm_loadu_ps(&dct[24]);
  line4 = _mm_loadu_ps(&dct[28]);
  
  char1 = _mm_loadu_si128((const __m128i*)&quant_tbl[16]);
  char2 = _mm_loadu_si128((const __m128i*)&quant_tbl[20]);
  char3 = _mm_loadu_si128((const __m128i*)&quant_tbl[24]);
  char4 = _mm_loadu_si128((const __m128i*)&quant_tbl[28]);

  line5 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char1));
  line6 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char2));
  line7 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char3));
  line8 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char4));
 
  line1 = _mm_div_ps (line1,line_a);
  line2 = _mm_div_ps (line2,line_a);
  line3 = _mm_div_ps (line3,line_a);
  line4 = _mm_div_ps (line4,line_a);

  
  line1 = _mm_div_ps (line1,line5);
  line2 = _mm_div_ps (line2,line6);
  line3 = _mm_div_ps (line3,line7);
  line4 = _mm_div_ps (line4,line8);

  line1 = _mm_round_ps(line1,_MM_FROUND_TO_NEAREST_INT);
  line2 = _mm_round_ps(line2,_MM_FROUND_TO_NEAREST_INT);
  line3 = _mm_round_ps(line3,_MM_FROUND_TO_NEAREST_INT);
  line4 = _mm_round_ps(line4,_MM_FROUND_TO_NEAREST_INT);
  	
  _mm_storeu_ps(&out_data[16],line1);
  _mm_storeu_ps(&out_data[20],line2);
  _mm_storeu_ps(&out_data[24],line3);
  _mm_storeu_ps(&out_data[28],line4);
  //E2

  //S3
  line_a = _mm_loadu_ps(&fours[0]);
  line1 = _mm_loadu_ps(&dct[32]);
  line2 = _mm_loadu_ps(&dct[36]);
  line3 = _mm_loadu_ps(&dct[40]);
  line4 = _mm_loadu_ps(&dct[44]);
  
  char1 = _mm_loadu_si128((const __m128i*)&quant_tbl[32]);
  char2 = _mm_loadu_si128((const __m128i*)&quant_tbl[36]);
  char3 = _mm_loadu_si128((const __m128i*)&quant_tbl[40]);
  char4 = _mm_loadu_si128((const __m128i*)&quant_tbl[44]);

  line5 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char1));
  line6 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char2));
  line7 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char3));
  line8 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char4));
 
  line1 = _mm_div_ps (line1,line_a);
  line2 = _mm_div_ps (line2,line_a);
  line3 = _mm_div_ps (line3,line_a);
  line4 = _mm_div_ps (line4,line_a);

  
  line1 = _mm_div_ps (line1,line5);
  line2 = _mm_div_ps (line2,line6);
  line3 = _mm_div_ps (line3,line7);
  line4 = _mm_div_ps (line4,line8);

  line1 = _mm_round_ps(line1,_MM_FROUND_TO_NEAREST_INT);
  line2 = _mm_round_ps(line2,_MM_FROUND_TO_NEAREST_INT);
  line3 = _mm_round_ps(line3,_MM_FROUND_TO_NEAREST_INT);
  line4 = _mm_round_ps(line4,_MM_FROUND_TO_NEAREST_INT);
  	
  _mm_storeu_ps(&out_data[32],line1);
  _mm_storeu_ps(&out_data[36],line2);
  _mm_storeu_ps(&out_data[40],line3);
  _mm_storeu_ps(&out_data[44],line4);
  //E3

  //S4
  line_a = _mm_loadu_ps(&fours[0]);
  line1 = _mm_loadu_ps(&dct[48]);
  line2 = _mm_loadu_ps(&dct[52]);
  line3 = _mm_loadu_ps(&dct[56]);
  line4 = _mm_loadu_ps(&dct[60]);
  
  char1 = _mm_loadu_si128((const __m128i*)&quant_tbl[48]);
  char2 = _mm_loadu_si128((const __m128i*)&quant_tbl[52]);
  char3 = _mm_loadu_si128((const __m128i*)&quant_tbl[56]);
  char4 = _mm_loadu_si128((const __m128i*)&quant_tbl[60]);

  line5 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char1));
  line6 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char2));
  line7 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char3));
  line8 = _mm_cvtepi32_ps (_mm_cvtepi8_epi32 (char4));
 
  line1 = _mm_div_ps (line1,line_a);
  line2 = _mm_div_ps (line2,line_a);
  line3 = _mm_div_ps (line3,line_a);
  line4 = _mm_div_ps (line4,line_a);

  
  line1 = _mm_div_ps (line1,line5);
  line2 = _mm_div_ps (line2,line6);
  line3 = _mm_div_ps (line3,line7);
  line4 = _mm_div_ps (line4,line8);

  line1 = _mm_round_ps(line1,_MM_FROUND_TO_NEAREST_INT);
  line2 = _mm_round_ps(line2,_MM_FROUND_TO_NEAREST_INT);
  line3 = _mm_round_ps(line3,_MM_FROUND_TO_NEAREST_INT);
  line4 = _mm_round_ps(line4,_MM_FROUND_TO_NEAREST_INT);
  	
  _mm_storeu_ps(&out_data[48],line1);
  _mm_storeu_ps(&out_data[52],line2);
  _mm_storeu_ps(&out_data[56],line3);
  _mm_storeu_ps(&out_data[60],line4);
  //E4
  
  
    /* Zig-zag and quantize */
  //out_data[zigzag] = (float) round((dct[1] / 4.0) / quant_tbl[zigzag]);
  
}


static void quantize_block_old(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];


    float dct = in_data[v*8+u];

    /* Zig-zag and quantize */
    out_data[zigzag] = (float) round((dct / 4.0) / quant_tbl[zigzag]);
  }
}

static void dequantize_block(float *in_data, float *out_data,
    uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];

    float dct = in_data[zigzag];

    /* Zig-zag and de-quantize */
    out_data[v*8+u] = (float) round((dct * quant_tbl[zigzag]) / 4.0);
  }
}

void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb2[i] = in_data[i]; }

  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);

  scale_block(mb2, mb);
  quantize_block(mb, mb2, quant_tbl);

  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }

}

void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb[i] = in_data[i]; }

  dequantize_block(mb, mb2, quant_tbl);
  scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);
  for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = mb[i]; }

}

/*
Used for testing
*/
void sad_block_8x8_empty(uint8_t *block1, uint8_t *block2, int stride, int *result)
{

}

/*
Original unparallel sad_block_8x8
*/
void sad_block_8x8_old(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  int u, v;

  *result = 0;

  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
      *result += abs(block2[v*stride+u] - block1[v*stride+u]);
    }
  }
}


//81ms
/*
SSE version 1.3 of sad_block. Total time 81ms, measured on the lab machine.
BUGS: None
TESTED: OK
############################SECTIONS###############################

               #######Write to stack memory######
Writes memory from uint8_t block1 and block2 to uint64_t dirty_hack1 and dirty_hack2. Memory address to
consenate is calculated stride*x [x =0-7]  
Reason for use: 
-Consenate memory for sse load. 
-Can use aligned load operators. 
-Memory performence 
per frame time:  20ms measured on the lab machine

                 #######Calculations#######
Do the actual calculations. Uses _mm_sad_epu: abs(x1-x2), and _mm_adds_epu: x1+x2. 
per frame time:  26ms measured on the lab machine

                    ######Writeout#######
writeout sum to uint16_t get_res, only 2 values contains actual data. 
per frame time:  11ms measured on the lab machine		    
*/
void sad_block_8x8_sse(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 
 
  __m128i sum;
  
  uint16_t get_res[8]  __attribute__ ((aligned (16)));
  uint64_t dirty_hack1[8]  __attribute__ ((aligned (16))), dirty_hack2[8]  __attribute__ ((aligned (16)));
 
  
  /*
   #######Write to stack memory######
  */
    // +20 ms
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  
  dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
  dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
  dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
  dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];
    
  /*
                     #######Calculations#######
*/  
  //+26ms  little to none differenc between aligend and unaligned load
  sum = _mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[0]),_mm_loadu_si128((__m128i*)&dirty_hack2[0]));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[2]),_mm_loadu_si128((__m128i*)&dirty_hack2[2]))); 
  sum = _mm_adds_epu16(sum,_mm_sad_epu8(_mm_loadu_si128((__m128i*)&dirty_hack1[4]),_mm_loadu_si128((__m128i*)&dirty_hack2[4])));
  sum = _mm_adds_epu16(sum,_mm_sad_epu8( _mm_loadu_si128((__m128i*)&dirty_hack1[6]),_mm_loadu_si128((__m128i*)&dirty_hack2[6])));

  
/*
######Writeout#######
*/
  //+6ms
  _mm_store_si128((__m128i*)&get_res[0],sum);
  
   //+5ms   
  res += get_res[0]; 
  res += get_res[4];
   
  *result = res;
  
  
  /*    
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  }
  */
}

//112ms 94ms hvis load flyttes ut igjen
/*
SSE version 1.4V2 of sad_block. 112ms total time(other functions included). Tested if loading directly from
memory was faster then writing to stack, then loading. As  only 8 bytes of the memory loaded, was needed 
in the calculations, and needed memory was separeted by stride bytes. Two loads was used to get a 16 bytes
vector.    
V1: 94ms total time (other functions included). Do't do the load in to uncak, but in to named SSE vars.
BUGS: None
TESTED: OK
############################SECTIONS###############################

               #######Load and Unpack######
Loads 16 bytes unalignd from memory to a XMM register, 8 bytes needed and 8 bytes garbage.
Uses  _mm_unpacklo_epi64 to consenate data, from to registers. 
                 #######Calculations#######
Do the actual calculations. Uses _mm_sad_epu: abs(x1-x2), and _mm_adds_epu: x1+x2. 
per frame time:  15ms measured on the lab machine

                    ######Writeout#######
writeout sum to uint16_t get_res, only 2 values contains actual data. 
per frame time:  11ms measured on the lab machine		    
*/

void sad_block_8x8_sse_1_4(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 

  __m128i sum,load1,load3,load5,load7,load9,load11,load13,load15;
  uint16_t get_res[8]  __attribute__ ((aligned (16)));
 									        

  /*
   // V1
  //+22 ms mere nå, tapte 20ms ved flytte load ned hit!!!
  load1 =_mm_loadu_si128((__m128i*)&block1[0]);  load2 =_mm_loadu_si128((__m128i*)&block1[stride_r]);
  load3 =_mm_loadu_si128((__m128i*)&block1[stride_r*2]);  load4 =_mm_loadu_si128((__m128i*)&block1[stride_r*3]);
  load5 =_mm_loadu_si128((__m128i*)&block1[stride_r*4]);  load6 =_mm_loadu_si128((__m128i*)&block1[stride_r*5]);
  load7 =_mm_loadu_si128((__m128i*)&block1[stride_r*6]);  load8 =_mm_loadu_si128((__m128i*)&block1[stride_r*7]);

  load9 =_mm_loadu_si128((__m128i*)&block2[0]);  load10 =_mm_loadu_si128((__m128i*)&block2[stride_r]);
  load11 =_mm_loadu_si128((__m128i*)&block2[stride_r*2]);  load12 =_mm_loadu_si128((__m128i*)&block2[stride_r*3]);
  load13 =_mm_loadu_si128((__m128i*)&block2[stride_r*4]);  load14 =_mm_loadu_si128((__m128i*)&block2[stride_r*5]);
  load15 =_mm_loadu_si128((__m128i*)&block2[stride_r*6]);  load16 =_mm_loadu_si128((__m128i*)&block2[stride_r*7]);
  */


  /*
   #######Load and Unpack######
  */ 
 //+15ms
   load1 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block1[0]),_mm_loadu_si128((__m128i*)&block1[stride_r])); //unpack 64 from 1 and 64 from 2 store in dest.
   load3 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block1[stride_r*2]),_mm_loadu_si128((__m128i*)&block1[stride_r*3])); 
   load5 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block1[stride_r*4]),_mm_loadu_si128((__m128i*)&block1[stride_r*5])); 
   load7 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block1[stride_r*6]),_mm_loadu_si128((__m128i*)&block1[stride_r*7])); 
   
   load9 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block2[0]),_mm_loadu_si128((__m128i*)&block2[stride_r])); //unpack 64 from 1 and 64 from 2 store in dest.
   load11 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block2[stride_r*2]),_mm_loadu_si128((__m128i*)&block2[stride_r*3])); 
   load13 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block2[stride_r*4]),_mm_loadu_si128((__m128i*)&block2[stride_r*5])); //unpack 64 from 1 and 64 from 2 store in dest.
   load15 = _mm_unpacklo_epi64(_mm_loadu_si128((__m128i*)&block2[stride_r*6]),_mm_loadu_si128((__m128i*)&block2[stride_r*7])); 


   /*
                    #######Calculations#######
   */
   //+15ms
   sum = _mm_sad_epu8(load1,load9);
   sum = _mm_adds_epu16(sum,_mm_sad_epu8(load3,load11)); 
   sum = _mm_adds_epu16(sum,_mm_sad_epu8(load5,load13));
   sum = _mm_adds_epu16(sum,_mm_sad_epu8(load7,load15));
   

   /*
                       ######Writeout#######
   */
  //+6ms
  _mm_store_si128((__m128i*)&get_res[0],sum);
  
   //+5ms   
  res += get_res[0]; 
  res += get_res[4];
    
  *result = res;

  
  /*
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  }
  */ 
}



#ifdef __AVX__ 
/*
AVX version 1.0V2 of sad_block, performs poorly compared to sse version, probable reason is sse-avx penalty.
V1 with alligend load, no time difference.
BUGS: None
TESTED: OK
############################SECTIONS###############################

               #######Write to stack memory######
Writes memory from uint8_t block1 and block2 to uint64_t dirty_hack1 and dirty_hack2. Memory address to
consenate is calculated stride*x [x =0-7]  
Reason for use: 
-Consenate memory for avx load. 
-Can use aligned load operators. 
-Memory performence 
per frame time:  27ms measured on the lab machine

                 #######Calculations#######
Do the actual calculations. Uses _mm256_sad_epu: abs(x1-x2), and _mm256_adds_epu: x1+x2. 
per frame time:  21ms measured on the lab machine

                    ######Writeout#######
writeout sum to uint16_t get_res, only 4 values contains actual data. 
per frame time:  16ms measured on the lab machine		    
*/
void sad_block_8x8_avx(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  
  
  register int res =0; 
  int stride_r = stride; //sparer ca 10 (401) ms, hvis gjort til reg, taper 3-4 (404) ms i forhold til dette 

  __m256i sum;

  uint16_t get_res[16]  __attribute__ ((aligned (32))); //used to read out result from AVX 
  uint64_t dirty_hack1[8], dirty_hack2[8];

  
/*
####Write to stack memory####
*/
    // +27 ms
  dirty_hack1[0] =((uint64_t*)&block1[0])[0]; dirty_hack1[1] =((uint64_t*)&block1[stride_r])[0];
  dirty_hack1[2] =((uint64_t*)&block1[stride_r*2])[0]; dirty_hack1[3] =((uint64_t*)&block1[stride_r*3])[0];
  dirty_hack1[4] =((uint64_t*)&block1[stride_r*4])[0]; dirty_hack1[5] =((uint64_t*)&block1[stride_r*5])[0];
  dirty_hack1[6] =((uint64_t*)&block1[stride_r*6])[0]; dirty_hack1[7] =((uint64_t*)&block1[stride_r*7])[0];
  
  dirty_hack2[0] =((uint64_t*)&block2[0])[0]; dirty_hack2[1] =((uint64_t*)&block2[stride_r])[0];
  dirty_hack2[2] =((uint64_t*)&block2[stride_r*2])[0]; dirty_hack2[3] =((uint64_t*)&block2[stride_r*3])[0];
  dirty_hack2[4] =((uint64_t*)&block2[stride_r*4])[0]; dirty_hack2[5] =((uint64_t*)&block2[stride_r*5])[0];
  dirty_hack2[6] =((uint64_t*)&block2[stride_r*6])[0]; dirty_hack2[7] =((uint64_t*)&block2[stride_r*7])[0];

 
/*
####Calcualations####
*/
 //+21ms
  //lddqu trenger ikke alignedm minne, men ingen forskjell i hastighet fra v1
  sum = _mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[0]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[0]));
  sum = _mm256_adds_epu16(sum,_mm256_sad_epu8( _mm256_lddqu_si256((__m256i*)&dirty_hack1[4]),_mm256_lddqu_si256((__m256i*)&dirty_hack2[4])));

  
/*
####writeout####
*/

  //+10ms
  _mm256_store_si256((__m256i*)&get_res[0],sum);
  
  //+6ms
  res += get_res[0]; 
  res += get_res[4]; 
  res += get_res[8]; 
  res += get_res[12];
  
  *result = res;
  
  /*
  int re;
  //Test
  sad_block_8x8_old(block1,block2,stride,&re);
  if(re != *result) {
  printf("BUG in sad_block re:%d  result:%d\n",re,*result);
  }
  */
}
#endif /* __AVX__ */


