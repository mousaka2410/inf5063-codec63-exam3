#ifndef C63_ME_H_
#define C63_ME_H_

#include "c63.h"

struct mv_buf_cut{
  int extraRowsAbove;
  int extraRowsBelow;
  int topRow, nRows, nCols;
  int8_t *mv_x, *mv_y;
  int8_t *dev_mv_x, *dev_mv_y;
  int color_component;
  bool gpu;
};

// Declaration
void c63_motion_estimate(struct c63_common *cm);

void c63_motion_compensate(struct c63_common *cm);

void c63_motion_compensate_cuda(struct c63_common *cm); 

void c63_start_partial_motion_estimate(struct c63_common *cm, int color_component,
      int topRow, int nRows, bool gpu, struct mv_buf_cut *mv_bufs);

void c63_finalize_partial_motion_estimate(struct c63_common *cm, struct mv_buf_cut *mv_bufs);

void c63_partial_motion_compensate(struct c63_common *cm, int color_component,
      int topRow, int nRows, bool gpu);

void cpu_me_block_8x8(int w, int h, int range, int mb_x, int mb_y,
		      uint8_t *orig, uint8_t *ref, struct macroblock *mb);

#endif  /* C63_ME_H_ */
