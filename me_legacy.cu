/* Motion estimation for 8x8 block */
static void me_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{ 
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int sw = right-left; // search width
  int sh = bottom-top; // search height
  dim3 threads = dim3(sw, sh); /* Create one thread for each 
                                  search-window position */

  int *sad_buf = (int*) malloc(sw*sh*sizeof(int));
  int *dev_sad;
  CUDA_ERROR( cudaMalloc(&dev_sad, sw*sh*sizeof(int)) );

  cuda_sad_block_8x8<<<1,threads>>>(orig + my*w+mx, ref + top*w+left, w, dev_sad);
  
  
  CUDA_ERROR( cudaMemcpy(sad_buf, dev_sad, sw*sh*sizeof(int), 
              cudaMemcpyDeviceToHost) );
  

  // for testing:
  int best_sad  = INT_MAX;
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      int sad = sad_buf[(y-top)*sw+(x-left)];
#define ME_TEST 0
#if ME_TEST
      int sad2;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad2);

      if (sad != sad2){
          printf("(%d, %d), %d ~ %d\n", x, y, sad, sad2);
      }
#endif
#undef ME_TEST
      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }
#define PRINT_SAD 0
#if PRINT_SAD
      printf("sad: (%d,%d) %d\n", mb_x, mb_y, best_sad);
#endif
#undef PRINT_SAD
  /*
  if(best_sad != min_sad->sad) {
    printf("best_sad: %d  != min_sad: %d\n", best_sad,min_sad->sad);
  }
  */

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */
  free(sad_buf);
  CUDA_ERROR( cudaFree(dev_sad) );

  mb->use_mv = 1;
}



/* Motion estimation for 8x8 block */
static void me_block_8x8_o(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *orig, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  int range = cm->me_search_range;

  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = cm->padw[color_component];
  int h = cm->padh[color_component];

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int x, y;

  int mx = mb_x * 8;
  int my = mb_y * 8;

  int best_sad = INT_MAX;

  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      
      int sad;
      sad_block_8x8(orig + my*w+mx, ref + y*w+x, w, &sad);

     

      if (sad < best_sad)
      {
        mb->mv_x = x - mx;
        mb->mv_y = y - my;
        best_sad = sad;
      }
    }
  }
  
#define PRINT_SAD 0
#if PRINT_SAD
      printf("sad: (%d,%d) %d\n", mb_x, mb_y, best_sad);
#endif
#undef PRINT_SAD
  

  /* Here, there should be a threshold on SAD that checks if the motion vector
     is cheaper than intraprediction. We always assume MV to be beneficial */

  /* printf("Using motion vector (%d, %d) with SAD %d\n", mb->mv_x, mb->mv_y,
     best_sad); */

  mb->use_mv = 1;
}

void c63_motion_estimate_o(struct c63_common *cm)
{
  /* Compare this frame with previous reconstructed frame */
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      me_block_8x8_o(cm, mb_x, mb_y, cm->curframe->orig->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->U,
          cm->refframe->recons->U, U_COMPONENT);
    }
  }
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
    {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}

/* Motion compensation for 8x8 block */
 __global__ void cuda_mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  /*
    curframe: struct frame in cm
    mbs: struct macrblock *mbs in struct frame
    color_component: 0,1,2
    padw: int[color_components:3]
*/
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];

  // Copy block from ref mandated by MV 
  int x, y;
  
  //printf("bottom: %d  right: %d\n",bottom-top,right-left);
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}
